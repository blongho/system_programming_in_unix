TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += thread

SOURCES += \
    SharedMem.cc \
    Random.cc \
    Helpers.cc \
    Main.cc \
    CircularQueue.cc

HEADERS += \
    SharedMem.h \
    Random.h \
    Helpers.h \
    CircularQueue.h

DISTFILES +=
