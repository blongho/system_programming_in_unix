#include "Random.h"
#include <climits>

Random::Random(const int &minval, const int &maxval)
    :min(minval), max(maxval){}

Random::Random()
    :min(INT_MIN), max(INT_MAX){}

int Random::getMin() const
{
    return min;
}

void Random::setMin(int value)
{
    min = value;
}

int Random::getMax() const
{
    return max;
}

void Random::setMax(int value)
{
    max = value;
}

int Random::generateInt() const
{
    std::random_device rd;
    std::mt19937 engine(rd());
    std::uniform_int_distribution<int> dist(min, max);
    return dist(engine);
}
