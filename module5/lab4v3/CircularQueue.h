/**
  * @file CircularQueue.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @brief Class definition of a FIFO circular queue
  *
  * @since 2018-10-19
  * */
#ifndef CIRCULARQUEUE_H
#define CIRCULARQUEUE_H
#include <iostream>
#include <pthread.h>
#include <semaphore.h>

const int MAX_QUEUE_SIZE = 10;
class CircularQueue
{
private:
    /**
     * @brief size The amount of items present in the circularQueue
     */
    size_t size;

    /**
     * @brief head Keeps tract of the head(front) of the queue.
     *          Elements are taken from here
     */
    int head;

    /**
     * @brief tail Keeps tract of the tail(rear) of the queue.
     *  Elemements are taken from here
     */
    int tail;

    /**
     * @brief elements A fixed sized array to hold the number of elements
     * in the queue
     */
    int elements[MAX_QUEUE_SIZE];


    bool initializeLock();
public:
    CircularQueue();
    /**
     * @brief enqueue Adds element at the tail and move the tail forward
     * @param element item to add
     */
    void enqueue(const int &element);

    /**
     * @brief dequeue Returns the first item added to the queue and move the
     * head forward
     * @param element the item to be added.
     */
    void dequeue(int & element);

    /**
     * @brief getSize The number of elements currently in the queue
     * @return the current size of the queue
     */
    size_t getSize() const;

    /**
     * @brief isEmpty Checks if the queue is empty or not
     * @return  true if size==0 otherwise false
     */
    bool isEmpty() const;

    /**
     * @brief isFull Checks if the queue is full or not
     * @return  true if size==MAX_SIZE otherwise false
     */
    bool isFull() const;

};

#endif // CIRCULARQUEUE_H
