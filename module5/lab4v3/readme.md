# Lab4 "Producer / Consumer"
This lab makes use of Semaphores to manage IPC using shared memory.

See [program specification](../labDescription.pdf)(in swedish)

## How to run the program
### Requirements
- Basic knowledge of unix command line
- Unix system (Linux/Mac)
- make version 4 or above
- g++ compiler version 7 or above

### Run the program
 If you want to build the project, import the project in Qt creator
    using the lab4v2.pro file

If comfortable with command line, run `make`. The program will build the
`lab4`

#### Usage
- `lab4` => runs the program with 5000 Producer/Consumer, no sleep
- `lab4 number` => runs lab4 with 'number' Producer/Consumer, no sleep
- `lab4 number 0|1` => runs lab4 with 'number' Producer/Consumer, sleeps
 randomly for 0 to 3 seconds for Producer or Consumer

 where
 `number` is an unsigned integer
