/**
  * @file Random.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @brief A helper class to generate random integers in a range specified by
  * the user
  * @since 2018-10-19
  * */
#ifndef RANDOM_H
#define RANDOM_H
#include <random>

class Random
{
private:
    /**
     * @brief min The minimum value expected.
     * Default : INT_MIN @see http://www.cplusplus.com/reference/climits/
     */
    int min;

    /**
     * @brief max The maximum value expected.
     * Default: INT_MAX @see http://www.cplusplus.com/reference/climits/
     */
    int max;
public:
    Random();
    Random(const int&minval, const int &maxval);
    /**
     * @brief generateInt Generate a random integer int range min - max
     * @return
     */
    int generateInt() const;
    int getMin() const;
    void setMin(int value);
    int getMax() const;
    void setMax(int value);
};

#endif // RANDOM_H
