#include "Helpers.h"
#include <string>
#include <iostream>
#include <exception>
#include "Random.h"
using std::string;
using std::cout;
using std::endl;
using std::cerr;
void usage(std::string msg, std::string prog)
{
    cerr <<  msg << "\nBad values entered.\nUsage: " << prog
          << " <producers>(optional) <shouldSleep>(0|1)(optional)"
          << endl;
}
std::pair<int, int> getCommands(int cmds, char *argv[])
{
    int iterations{-1};
    int shouldSleep{0};
    if(cmds == 1){
        iterations = 5000;
        shouldSleep = 0;
    }else if(cmds == 2){ // user specifies only iterations
        string producers = argv[1];
        try {
            iterations = std::stoi(producers);
            if(iterations < 1){
                string msg{ "Producer/Consumer value should be greater than 0"};
                usage(msg, argv[0]);
            }
        } catch (std::invalid_argument &) {
            iterations = -1;
            cerr << "Bad value for number of producers/consumers.Exiting.."
                 << endl;
        }
    }else if(cmds == 3){ // user attempts to want to sleep
        string producers = argv[1];
        string wantSleep = argv[2];
        try {
            iterations = std::stoi(producers);
            if(iterations < 1){
                string msg{ "Producer/Consumer value should be greater than 0"};
                usage(msg, argv[0]);
            }
        } catch (std::invalid_argument &) {
            iterations = -1;
            //usage("",argv[0]);
        }
        try {
            shouldSleep = std::stoi(wantSleep);
            if(shouldSleep < 0){
                string msg{"Value to decide sleep should be 0 or 1"};
            }
        } catch (std::invalid_argument &) {
            shouldSleep = -1;
        }
    }else{
        usage("",argv[0]);
    }
    return std::make_pair(iterations, shouldSleep);
}


void randomSleep()
{
    struct timespec tspec;
    tspec.tv_sec = Random(0,3).generateInt();
    tspec.tv_nsec = 0;
    //cout << "Sleeping for " << tspec.tv_sec << " seconds" << endl;
    if(nanosleep(&tspec, NULL) == -1){
        cerr << "Failed to sleep" << endl;
    }
}
