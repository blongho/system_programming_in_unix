/**
  *@file Helpers.h
  * @author Bernard Che Longho(lobe1602@student.miun.se)
  * @brief Helper class for lab4.
  * @since 2018-10-19
  * */
#ifndef HELPERS_H
#define HELPERS_H
#include <algorithm>
#include <string>

/**
 * @brief getCommands Get number of producer/consumer processes and a value to
 *                      denote if sleep should occure or not
 * @param cmds      The number of commands entered
 * @param argv      The arguments entered
 * @return          a value pair: first=number of producer/consumer,
 *                                second=sleep or not {1=sleep, 0=no sleep}
 *                  Defaults: 5000 0 if cmds==2
 */
std::pair<int, int> getCommands(int cmds, char *argv[]);
void usage(std::string prog, std::string msg="");

void randomSleep();
#endif // HELPERS_H
