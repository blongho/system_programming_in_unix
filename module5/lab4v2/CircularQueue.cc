#include "CircularQueue.h"

CircularQueue::CircularQueue()
    :size(0), head(0), tail(0){}

size_t CircularQueue::getSize() const
{
    return size;
}


bool CircularQueue::isEmpty() const
{
    return size == 0;
}

bool CircularQueue::isFull() const
{
    return size == MAX_QUEUE_SIZE;
}

void CircularQueue::enqueue(const int &element)
{
    if(!isFull()){
        elements[tail] = element;
        size++;
        tail = (tail + 1) % MAX_QUEUE_SIZE;
    }
}

void CircularQueue::dequeue(int & element)
{
    if(!isEmpty()){
        element = elements[head];
        size--;
        head = (head + 1) % MAX_QUEUE_SIZE;
    }
}


