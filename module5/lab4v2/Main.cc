#include <iostream>
#include <semaphore.h>
#include <wait.h>
#include <new>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include "CircularQueue.h"
#include "SharedMem.h"
#include "Random.h"
#include "Helpers.h"

const int BUFSIZE = 10;
const int minRan = 1000;
const int maxRan = 9999;
static CircularQueue * buffer;
static pthread_mutex_t  bufferlock = PTHREAD_MUTEX_INITIALIZER;
static sem_t * spaceAvail; /** prevent writing to a full buffer */
static sem_t * itemAvail; /** prevent reading from an empty buffer*/
SharedMem sharedMem;
using std::cout;
using std::endl;
using std::cerr;

/**
 * @brief initSemaphores Initialize the sempares
 * @return  true if successfully initialized otherwise false
 */
static bool initSemaphores();

/**
 * @brief produce The producer process
 * @return Returns true if everything works well.
 * @details
 * For everything to be fine, the buffer should be able to be controlled using
 * bufferlock and the consumer/producer semaphores need to tell each other when
 * they are done accessing the buffer
 *
 * If all is well, the producer prints the number produced on screen together
 * with the number of items currently present in the buffer
 *
 * The produced number is a random integer @see Random.h/Random.cc
 */
bool produce();

/**
 * @brief consume The Consumer process
 * @return  returns true if everything went on well otherwise false
 * @details
 * For everything to work out well, the same conditions as for the producer
 * must be fullfiled
 *
 * In addition, the Consumer prints to the screen what it has consumed together
 * with the current number of items in the buffer
 */
bool consume();

void createMemory();

void createBuffer();
/**
 * @brief destroySemaphores Destroy the semaphores using sem_destory()
 */
void destroySemaphores();

/**
 * @brief main The main application
 * @param args The number of arguments entered
 * @param argv The arguments entered
 * @return returns EXIT_FAILURE or EXIT_SUCCESS depending on the situation
 *
 * Usage:
 *  lab4 => runs the program with 5000 Producer/Consumer, no sleep
 *  lab4 number => runs lab4 with 'number' Producer/Consumer, no sleep
 *  lab4 number 0|1 => runs lab4 with 'number' Producer/Consumer, sleeps
 *                          for 0 to 3 seconds for Producer or Consumer
 *
 */

int main(int args, char* argv[])
{
    // Get the number of iterations and choice for sleep or not
    std::pair<int, int> values = getCommands(args, argv);

    if(values.first < 0 || values.second < 0){
        return EXIT_FAILURE;
    }
    int iterations = values.first;
    bool isSleepOn = values.second;
    cout <<  '\n' << argv[0] << " runs with " << iterations << " iterations "
          <<(isSleepOn? "WITH random sleep\n": "WITHOUT random sleep\n")
         << endl;


    createMemory();

    // attach the parent activity to shared memory
    if(sharedMem.attach() == -1){
        cerr << "Failed to attach parent activity to sharedMem" << endl;
        return EXIT_FAILURE;
    }

    createBuffer();

    // initialize the semaphores

    if(!initSemaphores()){
        destroySemaphores();
        cerr << "Failed to initialize the semaphores" << endl;
    }

    // start the two proces
    pid_t childpid;
    if ((childpid = fork()) == -1) {
        cerr << "Failed to fork child" << endl;
    }

    if (childpid == 0) { // Child process
        if(sharedMem.attach() == -1){ // attach child to shared memory
            cerr << "Faied to attach child process " << getpid() << " to "
                 << "sharedMem object" <<endl;
        }
        for (int producers = 0; producers < iterations; producers++) {
            if(!produce()) {
                cerr << "Unexpected error in Producer" << endl;
                break;
            }
            if(isSleepOn){
                randomSleep();
            }
        }
        if(sharedMem.detach() == -1){// detach from shared memory and exit
            cerr << "Failed to detach child process " << getpid() << " from"
                 << " sharedMem object" << endl;
        }
        exit(EXIT_SUCCESS);
    } else { // parent process
        for (int consumers = 0; consumers < iterations; consumers++) {
            if(!consume()) {
                cerr << "Unexpected error in Consumer" << endl;
                break;
            }
            if(isSleepOn){
                randomSleep();
            }
        }
        int wstatus;
        // wait for child processes
        if(wait(&wstatus) == -1){
            cerr << "Failed to wait for " << getpid() << endl;
            return EXIT_FAILURE;
        }
    }

    // detach parent from shared memory
    if(sharedMem.detach() == -1){
        cerr << "Failed to detach parent from sharedMem object" << endl;
        sharedMem.remove();
        return EXIT_FAILURE;

    }

    // free other resources
    destroySemaphores();
    sharedMem.remove();

    return 0;
}
// END OF MAIN PROGRAM
void createMemory(){
    size_t semSize = sizeof(sem_t*);
    size_t bufferSize = sizeof(CircularQueue*);
    size_t mutexSize = sizeof(pthread_mutex_t*);
    size_t memSize = 2*semSize + bufferSize + mutexSize;
    sharedMem.allocate(IPC_PRIVATE, memSize);
}

void createBuffer(){
    buffer = new ((CircularQueue *) sharedMem.getAddr()) CircularQueue();

    spaceAvail = new ((sem_t *) sharedMem.getAddr() + sizeof(CircularQueue *)) sem_t();

    itemAvail = new ((sem_t *) sharedMem.getAddr() + sizeof(CircularQueue *)
                     + sizeof(spaceAvail)) sem_t();
}

bool initSemaphores()
{
    if(sem_init(spaceAvail, 1 , 0)){
        return false;
    }
    if(sem_init(itemAvail, 1, BUFSIZE)){
        sem_destroy(spaceAvail);
        return false;
    }
    return true; // success
}

bool consume()
{
    int error;
    while((error = sem_wait(spaceAvail) == -1) && (errno == EINTR));
    if(error){
        cerr << "Unexpected error while waiting for items " << endl;
        return false;
    }
    if(buffer->isEmpty()){
        if(pthread_mutex_lock(&bufferlock) == -1){
            cerr << "Error locking the critical section" << endl;
            return false;
        }
    }

    int item;
    buffer->dequeue(item);
    cout << "\tCons: " << item << " nBuff=" << buffer->getSize() << endl;

    if(pthread_mutex_unlock(&bufferlock)){
        cerr << "Failed to unlock critical section" << endl;
        return false;
    }
    if(sem_post(itemAvail) == -1){
        cerr << "Error notiying the producer" << endl;
        return false;
    }
    return true;
}

bool produce(){
    int error;
    while(((error = sem_wait(itemAvail))== -1) && (errno == EINTR));
    if(error){
        cerr << "Error accessing items" << endl;
        return false;
    }
    if(buffer->isFull()){
        if(pthread_mutex_lock(&bufferlock)){
            cerr << "Failed to lock critical section" << endl;
            return false;
        }
    }

    const int item =  Random(minRan, maxRan).generateInt();
    buffer->enqueue(item);
    cout << "Prod: " << item << " nBuff=" << buffer->getSize() << endl;

    if(pthread_mutex_unlock(&bufferlock)){
        cerr << "Failed to unlock critical section" << endl;
        return false;
    }

    if(sem_post(spaceAvail) == -1){
        cerr << "Failed to notify consumer" << endl;
        return false;
    }
    return true;
}

void destroySemaphores()
{

    if (sem_destroy(spaceAvail) == -1) {
        std::cerr << "Failed to destory the semphore spaceAvail" << std::endl;
    }
    if (sem_destroy(itemAvail) == -1) {
        std::cerr << "Failed to destroy the semaphore itemAvail" << std::endl;
    }
}


