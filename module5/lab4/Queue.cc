#include "Queue.h"
#include <iostream>
template<typename T>
Queue<T>::Queue(){}


template<typename T>
Queue<T>::~Queue(){
    //std::cerr << "destructor called..." << std::endl;
}

template<typename T>
void Queue<T>::enqueue(const T &element){
    if(!isFull()){
        elements.push(element);
    }else{
        std::cerr << "Queue is full" << std::endl;
    }
}

template<typename T>
T Queue<T>::dequeue() {
    T element{}; // initializes T to the default value of its type
    if(!isEmpty()){
        element = elements.front();
        elements.pop();
    }else{
        std::cerr << "Queue is empty" << std::endl;
    }
    return element;
}

template<typename T>
bool Queue<T>::isFull() const{
    return length() == capacity();
}

template<typename T>
bool Queue<T>::isEmpty() const{
    return elements.empty();
}

template<typename T>
unsigned Queue<T>::length() const{
    return elements.size();
}

template<typename T>
unsigned Queue<T>::capacity() const{
    return MAX_QUEUE_SIZE;
}

template<typename T>
void Queue<T>::display(std::ostream&os){
    if(!isEmpty()){
        std::queue<T> copy = elements;
       while(!copy.empty()){
            os << copy.front() << " ";
            copy.pop();
       }
       os << std::endl;
    }else{
        std::cerr << "Queue is empty" << std::endl;
    }
}
