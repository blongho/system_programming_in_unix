/**
  * @file Helpers.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  *
  * @since 2018-10-02
  *
  * @brief  Header file for some global functions for lab3
  *
  * */
#ifndef HELPERS_H
#define HELPERS_H

/**
 * @brief getCommand Get the command passed by the user.
 *                  If bad commands or no commands are entered, print error and
 *                  show usage info
 * @param cmds      The number of arguments passed in the program
 * @param arvg      The arguments passed (if any)
 * @return          The command passed or an empty string.
 */
int iterations(int cmds, char **arvg);


/**
 * @brief generateInt Generate an integer between 0 and 9
 * @return a generated integer value
 */
int generateInt();

#endif // HELPERS_H
