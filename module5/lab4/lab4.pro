TEMPLATE = app
CONFIG += console c++11 -pthread
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    SharedMem.cc \
    Main.cc \
    Queue.cc \
    Helpers.cc

HEADERS += \
    SharedMem.h \
    Queue.h \
    Helpers.h
