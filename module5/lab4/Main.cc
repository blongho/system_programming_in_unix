/**
  * @file Main.cc
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @brief Main program that demonstrates the producer/consumer problem
  *         The producer creates random int and sends to a shared buffer
  *         The consumer reads the valuesas they arrive
  *         Each process prints to the screen what has happed
  * */
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <new>
#include <stdlib.h>
#include <semaphore.h>
#include "Queue.h"
#include "Queue.cc"
#include "Helpers.h"
#include "SharedMem.h"

typedef Queue<int> Buffer; /** shared buffer */
using std::cout;
using std::endl;
using std::cerr;

void * producer();

int main (int argc, char *argv[])
{
    //Queue<int> buffer;
    cout << generateInt() << endl;
    int producers = iterations(argc, argv);

    sem_t *spaceAvail, *itemAvail, *mutex;
    Buffer *buffer;
    int memSize = sizeof(buffer) + sizeof(spaceAvail) + sizeof(itemAvail)
            + sizeof(mutex);

    // Create memory
    SharedMem *sharedMem = new SharedMem(IPC_PRIVATE, memSize);

    buffer = new (sharedMem->getAddr())Buffer();

    // create the semaphores
    spaceAvail = new (sharedMem->getAddr() + sizeof(buffer)) sem_t();

    itemAvail = new (sharedMem->getAddr() + sizeof(buffer)
                     + sizeof(spaceAvail)) sem_t();

    mutex = new (sharedMem->getAddr()+ sizeof(buffer)
                 + sizeof(spaceAvail)) sem_t() + sizeof(itemAvail);

    // Attempt to initialize the semaphores
    if(sem_init(spaceAvail, 1, 10) == -1
            || sem_init(itemAvail, 1, 0) == -1
            || sem_init(mutex, 1 ,1) == -1){
        cerr << "Failed initializing the semaphores" << endl;
    }

    // The producer
    int numberProduced{};
    int counter;
    for(counter = 0; counter < producers; counter++){
        sem_wait(spaceAvail);
        numberProduced = generateInt();
        if(!buffer->isFull()){
            sem_wait(mutex);
            buffer->enqueue(numberProduced);
            sem_post(mutex);
        }


        // Write on screen what has been done so far
        cout << "Producer produced: " << numberProduced << ". Queue now has "
             << buffer->length() << " elements" << endl;
        sem_post(spaceAvail);
    }


    // The consumer
    pid_t child = 0;
    int numberReceived{};
    while(sem_wait(itemAvail) == -1){
        if(errno != EINTR){
            cerr << "Failed to lock semlock" << endl;
        }
    }


    for(counter = 0; counter < producers; counter++){
        if(child == fork()){
             if(!buffer->isEmpty()){
                sem_wait(mutex);
                numberReceived = buffer->dequeue();
                sem_post(mutex);
                cout << "Consumer received: " << numberReceived << ". Queu now has "
                     << buffer->length() << " elements" << endl;
                sem_post(itemAvail);
            }
        }
    }

    // clean memory
    sem_destroy(spaceAvail);
    sem_destroy(itemAvail);
    sem_destroy(mutex);
    delete buffer;
    delete spaceAvail;;
    delete itemAvail;
    delete mutex;
    delete sharedMem;

    return 0;
}
