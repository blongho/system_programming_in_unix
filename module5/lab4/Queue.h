/**
  * @file   Queue.h
  * @author Bernard Che Longho, lobe1602@student.miun.se
  * @since  2018-10-15
  * @brief  Header file for class declaration of a secular Queue
  *
  * */
#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>
#include <queue>


const int MAX_QUEUE_SIZE = 10;

template<typename T>
class Queue
{
private:
    std::queue<T> elements; /** The container to hold the elements */
    unsigned capacity() const; /** The max size of the queue */
public:
    Queue();
    ~Queue();

    /**
     * @brief enqueue Add an element at the back of the queue
     * @param indata  The item to be added
     */
    void enqueue(const T& element);

    /**
     * @brief dequeue Get an item at the back of the queue
     * @return  the item at the back of the queue
     */
    T dequeue();

    /**
     * @brief full Check if queue is empty
     * @return true if empty else false;
     */
    bool isFull() const;

    /**
     * @brief empty Checks that the queue is not empty
     * @return true if no item is in the queue, otherwise false
     */
    bool isEmpty() const;

    /**
     * @brief length Get the length of the queue (number of items in the queue)
     */
    unsigned length() const;

    /**
     * @brief display Display the items in the queue
     * @param os the ouput stream
     */
    void display(std::ostream&os = std::cout);

};

#endif // QUEUE_H
