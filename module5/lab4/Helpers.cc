#include "Helpers.h"
#include <string>
#include <iostream>
#include <exception>
#include <random>
using std::cerr;
using std::endl;


int iterations(int cmds, char **arvg)
{
    int iterations{};
    if(cmds == 1)
        iterations = 5000;

    else if(cmds == 2){
        std::string command = arvg[1];
        try{
            iterations = std::stoi(command);
        }catch(std::exception &e){
            cerr << "Bad value: " << command << " =>" << e.what() << endl;
        }
    }
    else{
        cerr << "Usage: " << arvg[0] << " <number>(optional)" << endl;
    }
    return iterations;
}


int generateInt()
{
    std::default_random_engine ren;
    std::uniform_int_distribution<int> distribution(0, 9);
    return distribution(ren);

}
