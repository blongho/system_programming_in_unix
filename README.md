# System Programming in UNIX/Linux Environment #



### What is this repository for? ###

This repository contains all the laboratory exercises for the course [dt065g](https://www.miun.se/utbildning/kurser/data-och-it/datateknik/datateknik-gr-b-systemprogrammering-i-unixlinux-75-hp/kursplan/) (swedish).

Version: 1.0

### How do I get set up? ###

I will submit all the projects as Qt project with the `Projectname.pro` file
for easy import.

You can build it on your own system by creating a new project and importing the
project files (excluding the `.pro` file) and then build.

The project is meant to work on UNIX/Linux environment as the title suggests so
if you are using another system, you should be away if some headers are not
recognized.

I am using ubuntu18.04 for all the projects.


### Contribution guidelines ###
Since all the work is meant to be done individually, if you want to review the work, do a fork and then send me a request for your fix (if there is any)

### Who do I talk to? ###
[Bernard Longho](@blongho)
* [lobe1602](mailto:lobe1602@student.miun.se) (student email) or
* [blongho](mailto:blongho02@gmail.com) (private email)
