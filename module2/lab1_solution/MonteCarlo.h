// MonteCarlo.h
// Lab 1 - Unix-processer, PI och Monte-Carlo
// Mittuniversitetet, Systemprogrammering i UNIX / Linux
// Johan Jörnesten (jojr1400)
// 2016-09-08

#ifndef MONTECARLO_H
#define MONTECARLO_H
#include <random>

/**
 * @brief Represents the MonteCarlo method for approximating PI.
 *
 * Approximates the value of PI by randomly generating numbers and seeing where they land on an imaginary square.
 * By comparing the number of 'darts' that lands within the imaginary circle using the circle's equation x^2+y^2=r^2 with those that doesn't, we can approximate the area of the circle.
 * Then, using that information together with the equation of the area of the circle, A=PI*r^2, we can approximate PI.
 *
 * @since 1.0
 */
class MonteCarlo{
private:

    /**< The radius of our circle */
    static constexpr double RADIUS = 0.5;

    /**< The number of dart-throws we are going to simulate */
    const int nThrows;

    /**< The random number generation */
    std::random_device rd;

    /**< The random number engine */
    std::mt19937 gen;

    /**< The random number distributer */
    std::uniform_real_distribution<double> dis;

    /**
     * @brief Gets the number of 'darts' that were thrown (generated) within the circle
     * @return int number of hits
     */
    int getHits();

    /**
     * @brief Checks if a dart is within the circle
     * @param dart_x the dart's x-pos
     * @param dart_y the dart's y-pos
     * @return true if it is, false if it's not
     */
    bool pointIsInsideCircle(double dart_x, double dart_y);

public:
    /**
     * @brief Constuctor.
     * @param nThrows the number of 'darts' to simulate throwing at the square
     */
    MonteCarlo(int nThrows): nThrows(nThrows), rd(), gen(rd()), dis(0,1) {}

    /**
     * @brief Approximates the value of pi using the number of throws defined in the constructor
     * @return double approximation of pi
     */
    double approximatePi();
};

#endif // MONTECARLO_H

