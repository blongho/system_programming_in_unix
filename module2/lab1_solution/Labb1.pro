TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    MonteCarlo.cpp \
    PiCalc.cpp

HEADERS += \
    MonteCarlo.h \
    PiCalc.h

DISTFILES +=

