// PiCalc.cpp
// Lab 1 - Unix-processer, PI och Monte-Carlo
// Mittuniversitetet, Systemprogrammering i UNIX / Linux
// 2016-09-08


#include <iostream>
#include <fstream>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <vector>
#include <sstream>
#include <dirent.h>
#include "MonteCarlo.h"
#include "PiCalc.h"

using namespace std;

int main(int argc, char* argv[])
{
    if(argc != 3) {
        std::cerr << "Invalid parameters!" << std::endl << "Syntax: PiCalc <NUMBER OF FORKS> <NUMBER OF THROWS PER FORK>" << std::endl;
        return 1;
    }

    int n_forks, n_throws;
    {
        std::stringstream ss;
        ss.exceptions(std::ios::failbit | std::ios::badbit);
        try {
            ss << argv[1] << ' ' << argv[2];
            ss >> n_forks >> n_throws;
        } catch(const std::ios_base::failure &e) {
            perror("Unable to parse arguments");
            return 1;
        }
    }


    pid_t child_pid;


    // Forks the process
    for(int i = 0; i < n_forks; ++i) {
        child_pid = fork();
        if(child_pid == -1) {
            perror("Unable to fork");
            return 1;
        }
        else if(child_pid == 0) break;
    }



    // If process is child to main process..
    if(child_pid == 0) {
        MonteCarlo mc(n_throws);
        try {
            writeLineToFile((std::to_string(getpid()))+"."+FILE_EXTENSION, std::to_string(mc.approximatePi()));
        } catch(const std::ios_base::failure &e) {
            perror("Unable to write to file as fork process");
            return 1;
        }
    } else {
        std::vector<std::string> approximations;
        while((child_pid = r_wait(NULL)) > 0) { // notice that thanks to r_wait we don't need to handle other errno (ECHILD is what we want)
            try {
                std::string approximated_pi = readFirstLineFromFile(std::to_string(child_pid)+"."+FILE_EXTENSION);
                approximations.push_back(approximated_pi);
                std::cout << "PID " << std::to_string(child_pid) << ": " << approximated_pi << std::endl;
            } catch(const std::ios_base::failure &e) {
                perror("Unable to read file");
            }
        }

        double sum = 0;
        double count = 0;
        for(auto approximation : approximations) {
            try {
               sum += std::stod(approximation);
               ++count;
            } catch(const std::invalid_argument &e) {
                std::cerr << "Unable to convert fork data to integer!" << std::endl;
            } catch(const std::out_of_range &e) {
                std::cerr << "Fork data is out of the range of acceptable values for type double!" << std::endl;
            }
        }

        if(sum > 0 && count > 0) {
            std::cout << "Average: " << sum/count << std::endl;
        } else {
            std::cerr << "Unable to approximate PI due to an invalid program state!" << std::endl;
            return -1;
        }

        clearTmp();
    }



    return 0;
}


void clearTmp() {
    DIR *dirPtr;
    struct dirent *direntPtr;

    dirPtr = opendir("./");
    if(dirPtr == NULL) {
        perror("Unable to open local directory");
        return;
    }

    errno = 0;
    while((direntPtr = readdir(dirPtr)) != NULL) {
        std::string dirEntName(direntPtr->d_name);
        if(dirEntName.find(FILE_EXTENSION) != string::npos) {
            if(unlink(direntPtr->d_name) == -1) {
                std::string error_msg((std::string)"Unable to delete file with name " + direntPtr->d_name);
                perror(error_msg.c_str());
            }
        }

        errno = 0;
    }

    if(errno != 0) perror("Problem processing tmp files");

}


void writeLineToFile(std::string filename, std::string output_line) {
    ofstream ostream;
    ostream.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    ostream.open(filename, ios::trunc | ios::out);
    ostream << output_line << std::endl;
    ostream.close();
}


std::string readFirstLineFromFile(string filename)  {
    ifstream istream;
    istream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    istream.open(filename, ios::in);
    std::string result;
    getline(istream, result);
    istream.close();
    return result;
}


pid_t r_wait(int *stat_loc)
{
    int retval;

    while(((retval = wait(stat_loc)) == -1) && (errno == EINTR));
    return retval;
}
