// PiCalc.h
// Lab 1 - Unix-processer, PI och Monte-Carlo
// Mittuniversitetet, Systemprogrammering i UNIX / Linux
// 2016-09-08


#ifndef MAIN_H
#define MAIN_H
#include <string>
#include <unistd.h>

/**< The file extension to use for IPC */
const std::string FILE_EXTENSION = "tmp";

/**
 * @brief Writes a line to a file.
 * @param the name of the file
 * @param output_line the string to store
 * @warning If file does not exist, it will afterwards. If file already exists, a new one will replace it.
 *
 * @throws std::ios_base::failure upon problem with IO
 */
void writeLineToFile(std::string filename, std::string output_line);

/**
 * @brief readLineFromFile
 * @param the name of the file
 * @return line from file
 *
 * @throws std::ios_base::failure upon problem with IO
 */
std::string readFirstLineFromFile(std::string filename);

/**
 * @brief Restarts wait until it hasn't been interrupted.
 * @param stat_loc the place to store the process's exit state;
 * @author Robbins K, Robbins S (Unix Systems Programming, Communication, Concurrency and Threads, Second Edition (ISBN: 0130424110))
 * @return the result of wait()
 */
pid_t r_wait(int *stat_loc);

/**
 * @brief Clears the program of temporary logs
 */
void clearTmp();

#endif // MAIN_H

