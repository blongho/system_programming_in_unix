// MonteCarlo.cpp
// Lab 1 - Unix-processer, PI och Monte-Carlo
// Mittuniversitetet, Systemprogrammering i UNIX / Linux
// 2016-09-08

#include "MonteCarlo.h"
#include <math.h>

double MonteCarlo::approximatePi()
{
    int nHits = getHits();
    double squareArea = this->RADIUS + this->RADIUS;
    double circleArea = squareArea*((double)nHits/this->nThrows);

    return circleArea / pow(this->RADIUS, 2);
}

int MonteCarlo::getHits() {
    int nHits = 0;
    for(int i = 0; i < this->nThrows; ++i) {
        double dart_x = dis(gen);
        double dart_y = dis(gen);
        if(pointIsInsideCircle(dart_x, dart_y)) {
            ++nHits;
        }
    }

    return nHits;
}

bool MonteCarlo::pointIsInsideCircle(double dart_x, double dart_y) {
    double distanceFromCenter = sqrt(pow(dart_x-0.5, 2) + pow(dart_y-0.5, 2));
    return distanceFromCenter < this->RADIUS;

}
