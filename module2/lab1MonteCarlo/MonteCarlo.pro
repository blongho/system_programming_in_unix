TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    RandomPair.cpp \
    EstimatePi.cpp \
    CommandLineHandler.cpp \
    Process.cpp

HEADERS += \
    RandomPair.h \
    EstimatePi.h \
    CommandLineHandler.h \
    Process.h
