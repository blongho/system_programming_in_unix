#include "CommandLineHandler.h"

CommandLineHandler::CommandLineHandler(const int& args, char *argv[]){
	process(args, argv);
}


void CommandLineHandler::process(const int& args, char *argv[]){
	int children{};
	int iterations{};
	if(args > 0 && args < 3){
		std::string prog = argv[0];
		prog = prog.substr(prog.rfind('/')+1);
		std::cerr << "Usage: " << prog << " arg1 arg2\n"
				  << "  targ1  number of child process(int)\n"
				  << "  targ2  number of iterations for the estimate(int)"
				  << std::endl;
	}else if(args == 3){
		std::string entry1{argv[1]};
		std::string entry2{argv[2]};
		try{
			children = std::stoi(entry1);
			iterations = std::stoi(entry2);
			valuePair = std::make_pair(children, iterations);
			goodParameters = true;

		}catch(std::exception &e){
			std::string prog = argv[0];
			prog = prog.substr(prog.rfind('/')+1);
			std::cerr << "Bad entry: " << e.what() << std::endl;
			std::cerr << "Usage: " << prog << " arg1 arg2\n"
					  << "  arg1  number of child process(int)\n"
					  << "  arg2  number of iterations for the estimate(int)"
					  << std::endl;
		}
	}else{
		std::string prog = argv[0];
		prog = prog.substr(prog.rfind('/')+1);
		std::cerr << "Exactly three arguments required\n"
				  << "Usage: " << prog << " arg1 arg2\n"
				  << "  arg1  number of child process(int)\n"
				  << "  arg2  number of iterations for the estimate(int)"
				  << std::endl;
	}
}

IntegerPair CommandLineHandler::getValues(){
	return valuePair;
}

bool CommandLineHandler::isGoodParameters(){
	return goodParameters;
}
