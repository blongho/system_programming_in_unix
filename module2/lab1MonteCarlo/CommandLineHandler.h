#ifndef COMMAND_LINE_HANDLER_H
#define COMMAND_LINE_HANDLER_H
#include <iostream>
#include <string>
#include <utility> // std::pair
typedef std::pair<int, int> IntegerPair;

class CommandLineHandler
{
private:
	bool goodParameters = false;
	IntegerPair valuePair{};
	void process(const int& args, char **argv);
public:
	CommandLineHandler() = default;
	CommandLineHandler(const int& args, char *argv[]);
	~CommandLineHandler() = default;
	bool isGoodParameters();
	IntegerPair getValues();
};

#endif // COMMANDLINE_H
