/**
  * @file RandomPair.h
  *
  * @author Bernard Che Longho (longberns@gmail.com)
  *
  * @brief A class to generate a pair of random numbers (double)
  * @since 2018-09-11
  * */

#ifndef RANDOMPAIR_H
#define RANDOMPAIR_H
#include <random>
#include <utility> // std::pair

/**
 * @typedef DoublePair
 * @brief An alias for a pair of doubles
 */
typedef std::pair<double, double> DoublePair;

class RandomPair
{
public:
    RandomPair() = default;

    /**
     * @brief generate Generate a pair of random numbers
     * @return a pair of doubles
     */
    DoublePair generate();
};

#endif // RANDOMPAIR_H
