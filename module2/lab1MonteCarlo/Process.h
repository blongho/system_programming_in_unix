#ifndef PROCESS_H
#define PROCESS_H
#include <fstream>
#include <iostream>
#include <iomanip>
class Process
{
private:
	int pid;
	double pi;
public:
	Process();
	Process(const int &_pid, const double &_pi);
	void saveProcessToFile();
	void readProcessFromFile(int pid);
	double getPi();
};

#endif // PROCESSMANAGER_H
