#include "Process.h"
Process::Process()
	:pid{}, pi{}{
}

Process::Process(const int &_pid, const double &_pi)
	:pid{_pid}, pi{_pi}{
}

void Process::saveProcessToFile(){
	std::ofstream ofs;
	ofs.open (std::to_string(pid),  std::ofstream::out | std::ofstream::app);

	if(ofs.is_open()){
		ofs << pid  << ' ' << pi << '\n';
		ofs.close();
	}else{
		std::cerr << "Error opening \"" << std::to_string(pid)
				  << "\"" << std::endl;
	}
}

void Process::readProcessFromFile(int pid){
	std::ifstream ifs;
	ifs.open(std::to_string(pid));
	if(ifs.is_open()){
		ifs >> pid >> pi;
		ifs.close();
	}else{
		std::cerr << "Error opening \"" << std::to_string(pid)
				  << "\"" << std::endl;
	}
}

double Process::getPi(){
	return pi;
}

