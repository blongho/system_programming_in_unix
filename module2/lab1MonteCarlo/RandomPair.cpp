#include "RandomPair.h"

DoublePair RandomPair::generate(){
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double>dis(0, 1);
    double x = dis(gen);
    double y = dis(gen);
    return std::make_pair(x, y);
}
