/**
 *@file EstimatePi.h
 *
 *@author Bernard Che Longho (longberns@gmail.com)
 *
 *@brief A class that estimates the value of pi
 *
 * @since 2018-09-11
 *
 * */
#ifndef ESTIMATEPI_H
#define ESTIMATEPI_H
#include "RandomPair.h"

class EstimatePi
{
private:
    int iterations; /** number of iterations that the program will run*/
    int circlePoints; /** points inside the circle {x,y:x^2 + y^2 <= 1}*/
    int squarePoints; /** points outside the circle (if not inside, then outside)*/
public:
    EstimatePi();
    EstimatePi(const int& iter);
    ~EstimatePi() = default;
    /**
     * @brief Check if two points are within a circle
     * @param x the x value
     * @param y the y value
     * @return bool for sum of the squares <= 1
     */
    bool isCirclePoint(const double &x, const double &y);

    /**
     * @brief getPi Does the heavy-lifting of this class.
     * Iterates ${iterations} and then calculate the pi value
     * @return the estimated value of pi after ${iterations} runs
     */
    double getPi();
};
#endif // ESTIMATEPI_H
