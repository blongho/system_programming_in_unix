#include "EstimatePi.h"

EstimatePi::EstimatePi()
    :iterations{},circlePoints{}, squarePoints{}{}

EstimatePi::EstimatePi(const int& iter)
    :iterations(iter), circlePoints{}, squarePoints{}{}



double EstimatePi::getPi(){
    RandomPair randomPair;
    DoublePair pair{};
    for(int i = 0; i < iterations; i++){
        pair = randomPair.generate();
        squarePoints++;
        if(isCirclePoint(pair.first, pair.second)){
            circlePoints++;
        }
    }
    return (4.0 * circlePoints)/squarePoints;
}

bool EstimatePi::isCirclePoint(const double &x, const double &y){
    return (x*x + y*y) <= 1;
}
