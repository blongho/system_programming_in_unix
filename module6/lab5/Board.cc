/**
  * @file   Board.cc
  * @brief  Implementation of Board.h
  * @since  2018-10-25
  * */
#include "Board.h"
#include <iomanip>
using std::setw;

void Board::reset()
{
    for(int row = 0; row < ROWS; row++){
        for(int col = 0; col < COLUMNS; col++){
            data[row][col] = EMPTY;
        }
    }
}

Board::Board()
{
    reset();
}


void Board::insert(const Point &p, const char &val)
{
    data[p.y][p.x] = val;
}

void Board::display(std::ostream &os)
{
    os << '\n';
    os << setw(2) << ' ';
    for(int col = 0; col < COLUMNS; col++){
        os << setw(4) << col;
    }
    os << '\n';

    for(int row = 0; row < ROWS; row++){
        os << setw(2) << row; // print the row numbers
        for(int col = 0; col < COLUMNS; col++){
            os << setw(4) << data[row][col];
        }
        os << '\n';
    }
}


std::ostream &operator<<(std::ostream&os, const Point&p){
    return os << "Point{" << p.x << ", " << p.y << "}";
}
