/**
  * @file    Main.cc
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @brief  Entry and exit point to the program of lab5
  * @since  2018-11-03
  *
  * Run:
  * lab5
  * */
#include "TestProgram.h"

int main()
{   
    try {
        TestProgram test;
        test.run();

    } catch (std::runtime_error &re) {
        perror(re.what());
        return 1;
    }
    return 0;
}


