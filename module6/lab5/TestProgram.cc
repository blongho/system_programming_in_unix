#include "TestProgram.h"
#include <exception>
#include <regex>
#include <random>

using std::cerr;
using std::endl;
using std::cout;
using std::getline;
using std::cin;

// program will end if connection cannot be established
TestProgram::TestProgram()
{
    try {
        client = Client(HOST, PORT);
    } catch (std::runtime_error &re) {
        throw std::runtime_error(re.what());
    }
    board = Board();
}

void TestProgram::initGame()
{
    clearScreen();
    cout << "\nWelcome to the tic-tac-to delux from luffarserv.miun.se\n" << endl;
    cout << "Your name: ";
    getline(cin, player);

    string tmpOpponent;
    bool goodOpponent = false;
    do{
        cout << player << ", choose opponent(0=striker, 1=Defender)?: ";
        getline(cin, tmpOpponent);
        if(tmpOpponent.size() != 1){
            cerr << "\nEnter 1 for Striker and 1 for Defender\n" << endl;
        }else if(tmpOpponent == "0" || tmpOpponent == "1"){
            goodOpponent = true;
        }else{
            cerr << "\nEnter 1 for Striker and 1 for Defender\n" << endl;
        }
    }while (!goodOpponent);

    setOpponent(tmpOpponent);

    starter = decideStarter(); // set starter 0 = human and 1 = ai

    string playerSize = player.size() < 10? "0" + std::to_string(player.size())
                                          : std::to_string(player.size());

    string initmsg = "CHA:" + tmpOpponent + ":" + std::to_string(starter) +
            ":" +  playerSize + ":" + player + "\n";

    client.sendMessage(initmsg);
}


int TestProgram::decideStarter()
{
    //Random generate who starts
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> distribution(0, 1);
    return  distribution(generator);
}


bool TestProgram::isHumanStart()
{
    return starter == 0;
}


std::string TestProgram::humanPlay()
{
    string cmd;
    bool goodCmd = false;
    do{
        cout << endl;
        cout << player << ", make a move 'x,y' or QUI to quit\nYour moves are"
                          "marked with '" << MARKER_HUMAN << "': ";
        getline(cin, cmd);
        if(cmd.substr(0,3) == "QUI"){ // user chooses to quit game
            goodCmd = true;
        }else{
            size_t commaPos = cmd.find(',');
            if(commaPos == std::string::npos){
                cerr << "Command must have a comma(,) separating x and y" << endl;
            }else{

                string xvalue = cmd.substr(0, commaPos);
                string yvalue = cmd.substr(commaPos + 1);

                // clear trailing spaces i.e return '04' if value is ' 04 '
                xvalue = regex_replace(xvalue, std::regex(" +"), "");
                yvalue = regex_replace(yvalue, std::regex(" +"), "");
                xvalue = xvalue.size() <2? "0"+xvalue: xvalue;
                yvalue = yvalue.size() <2? "0"+yvalue: yvalue;

                // Add trailing new line to command
                cmd = "MOV:" + xvalue + ":" + yvalue + "\n";
                //cout << cmd << endl;
                goodCmd = true;
            }
        }

    }while(!goodCmd);

    return cmd;
}

Point TestProgram::getPoint(std::string &msg)
{
    //std::string mov("MOV:00:00");
    Point point;

    if(!msg.empty() && msg != "QUI"){ // this is a move command
        std::string numbers = msg.substr(msg.find_first_of(':') + 1);
        std::string xvalue = numbers.substr(0, numbers.find(':'));
        std::string yvalue = numbers.substr(numbers.rfind(':') + 1);
        try {
            int x = std::stoi(xvalue);
            int y = std::stoi(yvalue);
            point = Point(x, y);
        } catch (std::exception &e) {
            cerr << e.what() << endl;
        }
    }else{
        point = Point(-1,-1);
    }
    return point;
}


void TestProgram::processHumanMove(std::string &move)
{
    if(move == "QUI"){
        endGame();
    }
    Point hpoint = getPoint(move);
    clearScreen();
    board.insert(hpoint, MARKER_HUMAN);
    board.display();
    client.sendMessage(move);
}


void TestProgram::processAiResponse(std::string &response)
{
    if(response.find("ILC") != std::string::npos){
        cerr << "Illegal command received." << endl;
        playing = false;
        endGame();
    }
    else if(response.find("ILM") != std::string::npos){
        cerr << "Illegal move" << endl;
        playing = false;
        endGame();
    }
    else if(response.find("WIN") != std::string::npos){
        if(response.find("MOV") != std::string::npos){
            winner = opponent;
        }else{
            winner = player;
        }
        //response = response.substr(0, response.size() - 2);
        cout << winner << " wins (" << response << ")!" << endl;
        playing = false;
        endGame();
    }
    else if(response.find("TCL") != std::string::npos){
        cerr << "I don't have time to waste. Game over!" << endl;
        //client.close();
        playing = false;
        endGame();
    }
    else if(response.find("NAP") != std::string::npos){
        cerr << "No player set. Game over." << endl;
        playing = false;
        endGame();
    }else if(response.find("MOV") != std::string::npos){
        Point aipoint = getPoint(response);
        clearScreen();
        cout << opponent << " move: " << getPoint(response) << endl;
        board.insert(aipoint, MARKER_AI);
        board.display();
    }
}

void TestProgram::setOpponent(const std::string &value)
{
    if(value == "0"){
        opponent = "Striker";
    }else if(value == "1"){
        opponent = "Defender";
    }
}

void TestProgram::endGame()
{
    cout << "Quiting the program..." << endl;
    client.close();
    exit(EXIT_SUCCESS);
}

void TestProgram::clearScreen()
{
    system("clear");
}

void TestProgram::run()
{
    initGame();
    int rounds{};
    string aiMessage; // ai message
    client.receiveMessage(aiMessage);
    if(!aiMessage.empty() && aiMessage[0] == 'O') // this is ok message
    {
        clearScreen();
        string gameResponse;
        std::string humanMove;
        do{
            if(isHumanStart()){
                if(rounds < 1){
                    cout << "You[" << player << "] starts" << endl;
                }
                if(!humanMove.empty()){
                    cout << player << " previous move: " << getPoint(humanMove)
                         << endl;
                }
                humanMove = humanPlay();
                processHumanMove(humanMove);
                client.receiveMessage(gameResponse);
                processAiResponse(gameResponse);
            }else{
                if(rounds < 1){
                    cout << opponent << " starts" << endl;
                }
                client.receiveMessage(gameResponse);
                processAiResponse(gameResponse);
                if(!humanMove.empty()){
                    cout << player << " previous move: " << getPoint(humanMove)
                         << endl;
                }
                humanMove = humanPlay();
                processHumanMove(humanMove);
            }
            rounds++;
        }while(playing);
    }else{
        perror("Communication to the server could not be established");
        endGame();
    }
}

