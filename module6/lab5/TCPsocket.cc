#include "TCPsocket.h"
#include <cstring> // strcpy
#include "Restart.h"
#include <iostream>
#include <exception>
using std::cout;
using std::endl;
using std::cerr;


TCPsocket::TCPsocket()
    :ipaddress(""), port(0000), iSockDesc(-1){
    //cout << "Constructor called "<< endl;
}

string TCPsocket::getIpaddress() const
{
    return ipaddress;
}

void TCPsocket::setIpaddress(const string &value)
{
    ipaddress = value;
}

in_port_t TCPsocket::getPort() const
{
    return port;
}

void TCPsocket::setPort(const in_port_t &value)
{
    port = value;
}

void TCPsocket::connect(char ipaddress[], in_port_t port)
{
    if((iSockDesc = u_connect(static_cast<u_port_t>(port), ipaddress)) == -1){
        throw std::runtime_error("Failed to create the socket file "
                                 "descriptor");
    }
    this->ipaddress = ipaddress;
    this->port = port;
    //cout << "Server file descriptor " << iSockDesc << endl;
}

void TCPsocket::connect(const std::string &ipaddress, in_port_t port)
{
    const size_t n = ipaddress.size();
    char ip[n];
    strcpy(ip, ipaddress.c_str());
    if((iSockDesc = u_connect(static_cast<u_port_t>(port), ip)) == -1){
        throw std::runtime_error("Failed to create the socket file "
                                 "descriptor");
    }
    this->ipaddress = ipaddress;
    this->port = port;
    //cout << "Server file descriptor " << iSockDesc << endl;
}


void TCPsocket::close()
{
    if(iSockDesc != -1){
        if(r_close(iSockDesc) == -1){
            throw std::runtime_error("Failed to close the sockect file descriptor");
            //perror("Failed to close the sockect file descriptor");
        }else{
            cout << "Socket file descriptor closed successfully." << endl;
        }
    }else{
        cerr <<"Socket file descriptor not created. Nothing to destroy." << endl;
    }
}

