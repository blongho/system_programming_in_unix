TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += -lnsl

SOURCES += \
    Restart.cc \
    uici.cc \
    uiciname.cc \
    TCPsocket.cc \
    Board.cc \
    Main.cc \
    Client.cc \
    TestProgram.cc

HEADERS += \
    Restart.h \
    uici.h \
    uiciname.h \
    TCPsocket.h \
    Board.h \
    Client.h \
    TestProgram.h
