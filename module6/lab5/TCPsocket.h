/**
  * @file   TCPsocket.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @brief  Wrapper class to make a socket over TCP
  * @details
  *     Use:
  *        (1) (<TCPsocket>"::"<TCPsocket>"("<ipaddr, port>")" instantiates the classs with the server
  *                 ipadrress(ipaddrs) and the listening port (port)
  *        (2) <TCPsocket>"()" instantiates the class with server ip and port set to
  *                     #DEFAULT_IP and #DEFAULT_PORT, respectively
  *         If (1) is used, call ${connect()} to establish the connection and
  *             the socket file descriptor #iSockDesc
  *         if (2) is used, call ${connect
  *
  *
  * @since 2018-10-23
  * */
#ifndef TCPSOCKET_H
#define TCPSOCKET_H
#include <netinet/in.h>
#include <string>
#include "uici.h"

using std::string;

class TCPsocket
{
private:
    string ipaddress;  /** the ipaddress of the server string or dot notation*/
    in_port_t port;    /** the port which the server listens from*/

    int iSockDesc;  /** socket descriptor */
public:
    /**
     * Constructor
    * Creates the socket, may throw runtime_error.
    */
    TCPsocket();

    ~TCPsocket() = default;
    /** Connects to server using a hostname or
    * a dotted decimal address.
    * May throw runtime_error
    */
    void connect(char ipaddress[], in_port_t port);
    void connect(const std::string &ipaddress, in_port_t port);

    /**
     * Returns the socket descriptor
     */
    int getDescriptor() const {return iSockDesc;}
    /**
     * Closes the socket.
    * May throw runtime_error .
    */
    void close();

    string getIpaddress() const;
    void setIpaddress(const string &value);
    in_port_t getPort() const;
    void setPort(const in_port_t &value);
};
#endif // TCPSOCKET_H
