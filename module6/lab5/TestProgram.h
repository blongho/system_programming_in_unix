/**
  * @file   TestProgram.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @since  2018-11-03
  *
  * @brief  The test program for lab5
  * After complilation run lab5
  * */
#ifndef TESTPROGRAM_H
#define TESTPROGRAM_H
#include "Client.h"
#include "Board.h"

class TestProgram
{
private:
    const in_port_t PORT = 6500; // the tcp connection port
    const std::string HOST = "luffarserv.miun.se"; // the tcp host server addr
    const char MARKER_HUMAN = 'X'; // marker on the board for human moves
    const char MARKER_AI = 'O'; // marker on the for AI moves
    Client client;
    Board board; // the game board
    bool playing = true;
    std::string winner; // the winner of the game
    std::string player;  // the name of the player
    std::string opponent; // the opponent {Striker | Defender}
    int starter; // Get the starter {0=ai, 1=human} randomly generated

    /**
     * @brief initGame Initialize the game
     * Here the user
     * - enters the s(he) name
     * - chooses an opponent
     * - the function beginner() is called that chooses the beginner
     * - the user sends the request to start a game to the server
     */
    void initGame();
    /**
     * @brief getPoint Process a MOV:x:y command and get the values of x and y
     * @param msg the move command passed
     * @return a Point(x, y)
     */

    /**
     * @brief decideStarter decide who makes the first move.
     * @return randomly generated value 0 for human and 1 for ai
     */
    int decideStarter();

    /**
     * @brief isHumanMove check if the starter is human or not
     * @return returns true $starter == 0
     */
    bool isHumanStart();

    /**
     * @brief humanPlay The human is asked to enter the values of x,y or QUI
     * @return a well formatted string MOV:xx:yy or QUI
     */
    std::string humanPlay();

    /**
     * @brief getPoint Process the information in msg and extract the x and y
     * @param msg a message to be processed. Typically MOV:00:00
     * @return {x,y} or {-1,-1}
     */
    Point getPoint(std::string &msg);

    /**
     * @brief processHumanMove quit the game if move==QUI, otherwise the point
     * from the move and insert that into the board.
     *
     * Send the message to the server
     * @param move the human move obtained from humanPlay()
     */
    void processHumanMove(std::string &move);

    /**
     * @brief processAiResponse Process the information from the server and
     * print the appropriate response. If response contains
     * - ILC -> illegal command. End game
     * - ILM -> illegal move. End game
     * - WIN -> win. Print winner. End game
     * - TCL -> time out. End game
     * - NAP -> no player set. End game
     * - MOV -> getPoint and insert into board
     * @param response the response string from the server
     */
    void processAiResponse(std::string &response);

    void setOpponent(const std::string &value);

    [[noreturn]] void endGame(); // close the socket and end the game

    void clearScreen(); // auxilliary function to clear the console

public:
    /**
     * @brief TestProgram Initializes and oppens the client
     * initializes the board
     * If client connection fails, a runtime error is thrown
     */
    TestProgram();
    void run();
};

#endif // TESTPROGRAM_H
