TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    Random.cc \
    Restart.cc \
    TestTask.cc \
    Main.cc \
    Helpers.cc

HEADERS += \
    Random.h \
    Restart.h \
    TestTask.h \
    Helpers.h
