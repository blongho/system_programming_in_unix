/**
  * @file   Main.cc
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @since  2018-10-02
  *
  * @brief  A program that measures the realtime, virtual time and prof time of
  *         a process. It uses setitimer/getitimer to measure the times and
  *         signals and signal handlers to print information about the times and
  *         terminate the program
  *
  *         Different tasks are run based on the user's input from the cli
  * */
#include <signal.h>
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <memory>
#include <sys/time.h>
#include "TestTask.h"
#include "Helpers.h"

using std::shared_ptr;
using std::make_shared;
using std::cout;
using std::endl;
using std::cerr;
using std::setprecision;
using std::fixed;

// define global variables
/**
 * @brief doneflag The flag that indicates that the process is done
 * This is set global because we can not set this at run-time
 */
static volatile sig_atomic_t doneflag = 0;

const long double MILLION = 1000000L;

// timer variables
struct itimerval oldValue;      /** Time since program started*/
struct itimerval realTime;      /** The real time */
struct itimerval virtualTime;   /** The vi./Lab3 cpu
rual time*/
struct itimerval profTime;      /** The profTime*/

// calculculated times in seconds
long double rTime;      /** real time difference*/
long double vTime;      /** virtual time difference*/
long double pTime;      /** prof time difference*/


/**
 * @brief handleSignal Handle the signal sent to the program/process
 * @param signum The signal number (int)
 */
void handleSignal(int signum);

/**
 * @brief reportTimes Print information about the time that the program has been
 *          running.
 */
void reportTimes();

/**
  * The file containing the texts that will will processed by the I/O-bound
  * task.
  * If running the program as lab3 command
  *     Put this file in the same directory where the program files are present
  * If running the program as Qt project using Qt Creator
  *     Place this file in the build directory of project
  * */
const string startfile{"Daffodils.txt"};

/**
 * @brief makeTask create task based on command entered by user
 * @param command the command recevied [io|cpu|cpu-io]
 * @return nullptr|TestTask* a test task if on./Lab3 cpu
e of expected commands are
 *          entered otherwise, a nullptr
 */
shared_ptr<TestTask> createTask(string command);

//################## END OF MAIN ##############################################
int main(int args, char **argv)
{
    // Get the commands. If bad values, program ends and prints error
    string command = getCommand(args, argv);
    if(command.empty()){
        return 1;
    }

	// initialize old time
    oldValue.it_interval.tv_sec = 0;
    oldValue.it_interval.tv_usec = 0;
    oldValue.it_value.tv_sec = MILLION;
    oldValue.it_value.tv_usec = 0;

    // set the timers
    if(setitimer(ITIMER_REAL, &oldValue, nullptr) == -1){
        cerr << "Failed to set the real timer" << endl;
        return 1;
    }
    if(setitimer(ITIMER_VIRTUAL, &oldValue, nullptr) == -1){
        cerr << "Failed to set the virtual timer" << endl;
        return 1;
    }
    if(setitimer(ITIMER_PROF, &oldValue, nullptr) == -1){
        cerr << "Failed to set the virtual timer" << endl;
        return 1;
    }

    // Wait for signals and handle them accordingly
    signal(SIGUSR1, handleSignal);
    signal(SIGUSR2, handleSignal);

    // Added extra CTRL + C handling because i don't want the program to
    // terminate that way
    signal(SIGINT, handleSignal);

    // Create the task and start never-ending program
    shared_ptr<TestTask> task = createTask(command);
    if(task){
        while(!doneflag){
            task->run();
        }
    }else{
        cerr << "Unexpected error occured while creating required task" << endl;
        return 1;
    }

    return 0;
}
//################## END OF MAIN ##############################################


void reportTimes(){
    cout <<"Real time: " <<setprecision(2) << fixed<< rTime << " sec, " << 100.0 <<" %\n"
        <<"Virtual time: " << vTime << " sec, "<< vTime*100/rTime <<  " %\n"
        << "Prof time: " <<pTime << " sec, " << pTime*100/rTime << " %"
        << endl;
}

void handleSignal(int signum){
    cout << "\nSignal: " << signum << " received" << endl;

    // handle signal
    if(getitimer(ITIMER_REAL, &realTime) == -1){
        cerr << "Failed to get real timer" << endl;
        exit(EXIT_FAILURE);
    }
    if(getitimer(ITIMER_VIRTUAL, &virtualTime) == -1){
        cerr << "Failed to get real timer" << endl;
        exit(EXIT_FAILURE);
    }
    if(getitimer(ITIMER_PROF, &profTime) == -1){
        cerr << "Failed to get real timer" << endl;
        exit(EXIT_FAILURE);
    }

    rTime = MILLION * (oldValue.it_value.tv_sec - realTime.it_value.tv_sec) +
            oldValue.it_value.tv_usec - realTime.it_value.tv_usec;

    vTime = MILLION * (oldValue.it_value.tv_sec - virtualTime.it_value.tv_sec) +
            (oldValue.it_value.tv_usec - virtualTime.it_value.tv_usec);

    pTime = MILLION * (oldValue.it_value.tv_sec - profTime.it_value.tv_sec) +
            (oldValue.it_value.tv_usec - profTime.it_value.tv_usec);

    // Get the values back to seconds
    rTime /= MILLION * 1.0L;
    vTime /= MILLION * 1.0L;
    pTime /= MILLION * 1.0L;

    switch (signum)
    {
    case SIGUSR1:
        reportTimes();
        break;
    case SIGUSR2:
        reportTimes();
        doneflag = 1;
        break;
    case SIGINT:
        cerr << "\nOpen another terminal and type 'kill -SIGUSR1|-SIGUSR2' "
             << getpid() << endl;
        break;
    }
}


shared_ptr<TestTask> createTask(string command){
    // create the desired task
    if(command == "io")
        return make_shared<IOtask>(doneflag, startfile);
    else if(command == "cpu")
        return make_shared<CPUtask>(doneflag);
    else if (command == "cpu-io")
        return make_shared<CPUandIOtask>(doneflag, startfile);
    else return nullptr;
}
