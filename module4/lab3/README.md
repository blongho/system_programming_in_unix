# Lab3 - Signals and Interval timers
## Objective
Understand and use signals in unix environment and to use the itimerval
timers in the system to time processes.

## Task
Write a program that can measure the execution times of the processes and
manage the program flow using signals.

The times measured are
- the real time
- the virtual time (cpu time)
- the user + kernal time (prof time)

## Requirements
- Use `setitimer, getitimer, signals` and signal handlers to manage the
program flow.

### Program execution
- `Lab3 io ` runs an I/O-bound task (`IOtask`)
- `Lab3 cpu` runs a CPU-bound task (`CPUtask`)
- `Lab3 cpu-io` runs a mixture of cpu and i/o -bound tasks(`CPUandIOtask`)

Program should handle both signals from the timers and from the user
(SIGUSR1 && SIGUSR2)

If program receives user-defined signal, it should behave as follows
 - `SIGUSR1` received
  - print information about the various times and continue Running
 - `SIGUSR2` received
  - print information about the various times and let the program gracefully
  exits (no use use of `exit()`) by setting the `donflag` from the
  `TestTask` class.

### Program code supplied
`TestTask.h` contains prototypes of three tasks
- `IOtask` that does some heavy I/O operations
- `CPUtask` that does some heavy CPUtask
- `CPUandIOtask` that does a combination of i/o and cpu tasks

`TestTask.cc` contains the implementation of these classes

`Random.h, Random.cc` is helper class for CPUtask (generates random numbers
  between 0 and 1)
