# My observations
The main difference between them is the virtual time.
For the cpu-bound task (`CPUtask`), the virtual and pro times are the same.

By definition, the protime is cpu time plus time that the kernel
spent on the process. If these times are the same, this might be that the
process does not use any kernel resources.

On notes that the virtual time differs considerably with the real and
protimes for the I/O-bound task (`IOtask`). This is logical as a process
might require very little of the cpu resource expecially if the program
counter is not too involved in the execution of the process.

A sample run of the programs is shown below.

Running the CPU-bound task
```
./Lab3 cpu
PID 14592: CPUtask making random numbers...

Signal: 10 received
Real time: 23.75 sec, 100.00 %
Virtual time: 23.74 sec, 99.99 %
Prof time: 23.74 sec, 99.99 %

Signal: 10 received
Real time: 41.46 sec, 100.00 %
Virtual time: 41.46 sec, 99.99 %
Prof time: 41.46 sec, 99.99 %

Signal: 12 received
Real time: 78.41 sec, 100.00 %
Virtual time: 78.40 sec, 99.99 %
Prof time: 78.40 sec, 99.99 %
```
Running the I/O-bound tasks
```
./Lab3 io
PID 14676: IOtask copying files...

Signal: 10 received
Real time: 13.75 sec, 100.00 %
Virtual time: 3.93 sec, 28.57 %
Prof time: 12.73 sec, 92.57 %

Signal: 10 received
Real time: 33.59 sec, 100.00 %
Virtual time: 9.70 sec, 28.87 %
Prof time: 31.20 sec, 92.90 %

Signal: 12 received
Real time: 40.82 sec, 100.00 %
Virtual time: 11.76 sec, 28.82 %
Prof time: 37.98 sec, 93.03 %
```
Running the CPUandIOtask task
```
./Lab3 cpu-io
PID 15881: CPUandIOtask copying files and making random numbers...

Signal: 10 received
Real time: 19.35 sec, 100.00 %
Virtual time: 14.56 sec, 75.23 %
Prof time: 19.08 sec, 98.58 %

Signal: 10 received
Real time: 45.87 sec, 100.00 %
Virtual time: 34.49 sec, 75.19 %
Prof time: 45.28 sec, 98.70 %

Signal: 12 received
Real time: 56.98 sec, 100.00 %
Virtual time: 43.02 sec, 75.51 %
Prof time: 56.27 sec, 98.76 %
```
