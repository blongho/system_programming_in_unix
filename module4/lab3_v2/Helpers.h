/**
  * @file Helpers.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  *
  * @since 2018-10-02
  *
  * @brief  Header file for some global functions for lab3
  *
  * */
#ifndef HELPERS_H
#define HELPERS_H
#include <string>
using std::string;

/**
 * @brief getCommand Get the command passed by the user.
 *                  If bad commands or no commands are entered, print error and
 *                  show usage info
 * @param cmds      The number of arguments passed in the program
 * @param arvg      The arguments passed (if any)
 * @return          The command passed or an empty string.
 */
string getCommand(int cmds, char **arvg);

extern struct itimerval oldValue, realTime, virtualTime, profTime;
extern long double rTime, vTime, pTime;
#endif // HELPERS_H
