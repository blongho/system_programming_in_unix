#include "Helpers.h"
#include <iostream>
using std::cerr;
using std::endl;

string getCommand(int cmds, char **arvg)
{
    string command = "";
    if(cmds == 2){
        string tmp = arvg[1];
        if(tmp == "io" || tmp == "cpu" || tmp == "cpu-io"){
            command = tmp;
        }
        else{
            cerr << "Usage: " << arvg[0] << " [option]\n\n"
                 << "Options  Explanation\n--------------------\n"
                 << "  io     Run I/O-bound program\n"
                 << "  cpu    Run CPU-bound program\n"
                 << "  cpu-io Run a combination of CPU and IO operations" << endl;
        }
    }
    else{
        cerr << "Usage: " << arvg[0] << " [option]\n\n"
             << "Option   Explanation\n----------------------\n"
             << "  io     Run I/O-bound program\n"
             << "  cpu    Run CPU-bound program\n"
             << "  cpu-io Run a combination of CPU and IO operations" << endl;
    }

    return command;
}

