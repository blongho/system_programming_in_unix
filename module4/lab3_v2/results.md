## Results of my application
## Running the I/O-bound tasks
```
lab3 io

Try to kill me from another terminal with 'kill -10|-12 22789'
PID 22789: IOtask copying files...

Signal SIGUSR1 received
Real time: 25.48 sec, 100.00 %
Virtual time: 15.28 sec, 59.97 %
Prof time: 24.00 sec, 94.22 %

Signal SIGUSR2 received
Real time: 50.51 sec, 100.00 %
Virtual time: 30.85 sec, 61.08 %
Prof time: 48.67 sec, 96.37 %
```
Here, one sees that the there is a considerable difference between the
virtual time and the real and prof times.
By definition, the virtual time measures only the time that the process uses
for itself whereas the real time is the time that the process uses plus time
that it spends making system calls. The prof time is the time the process
uses as well as that spent in making system calls. One sees from the io-bound
task that it spends much time to itself.

## Running the cpu-bound task

```
lab3 cpu

Try to kill me from another terminal with 'kill -10|-12 22816'
PID 22816: CPUtask making random numbers...

Signal SIGUSR1 received
Real time: 36.59 sec, 100.00 %
Virtual time: 35.97 sec, 98.30 %
Prof time: 36.02 sec, 98.45 %

Signal SIGUSR2 received
Real time: 43.81 sec, 100.00 %
Virtual time: 43.05 sec, 98.26 %
Prof time: 43.11 sec, 98.40 %
```
From the results one can see that the CPUtask uses lots of the cpu resources.
This has a direct effect to the virtual time too since this time spent by
the processs doing system calls is also reflected to the time the program
spends on itself.

```
lab3 cpu-io

Try to kill me from another terminal with 'kill -10|-12 22898'
PID 22898: CPUandIOtask copying files and making random numbers...

Signal SIGUSR1 received
Real time: 35.17 sec, 100.00 %
Virtual time: 29.84 sec, 84.84 %
Prof time: 34.52 sec, 98.16 %

Signal SIGUSR2 received
Real time: 41.25 sec, 100.00 %
Virtual time: 34.97 sec, 84.79 %
Prof time: 40.48 sec, 98.15 %
```
With this combination of tasks one notices that the virtual time has increased
considerable compared to the io-bound. Based on the explanations give for the
other tasks, one is not surprised by this results. 
