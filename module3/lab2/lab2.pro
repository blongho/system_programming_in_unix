TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    Calculator.cc \
    CommandLineHandler.cpp \
    eCalculator.cc \
    MultiLengthInteger.cc \
    PiCalculator.cc \
    Restart.cc

HEADERS += \
    Calculator.h \
    CommandLineHandler.h \
    eCalculator.h \
    MultiLengthInteger.h \
    PiCalculator.h \
    Restart.h
