/**
 * @file  CommandLindHandler
 * @author Bernard Che Longho (lobe1602@student.miun.se)
 * @brief A Class that takes the arguments from the terminal and extracts the
 * values
 * @since 2018-09-23
 *
 * */
#ifndef COMMAND_LINE_HANDLER_H
#define COMMAND_LINE_HANDLER_H
#include <iostream>
#include <string>
#include <utility> // std::pair
#include <map>
typedef std::pair<int, int> IntegerPair;
using std::string;
class CommandLineHandler
{
private:
    bool goodParameters = false; /** Checks if an argument is passed */
    IntegerPair valuePair{}; /** get pair of input values */
    /**
     * @brief process Process the command line arguments
     * @param args the number of arguments passed (int)
     * @param argv the arguments passed (as string)
     */
    void process(const int& args, char **argv);

    /**
     * @brief printError Print errors that arise from the processing of the cli
     * @param prog the executable
     * @param args possible args as needed by the user/program
     */
    void printError(const std::string prog, std::map<std::string, std::string> args);

    /**
     * @brief errorMap A map that holds a needed argument and its associated meaning
     */
    std::map<std::string, std::string> errorMap;

public:
	CommandLineHandler() = default;
    /**
     * @brief CommandLineHandler
     * @param args @see ${process()}
     * @param argv @see ${process()}
     */
	CommandLineHandler(const int& args, char *argv[]);
	~CommandLineHandler() = default;
    /**
     * @brief isGoodParameters
     * @return true if the arguments have been entered as expecte
     */
	bool isGoodParameters();

    /**
     * @brief getValues Gets the arguments that are required
     * @return a pair of arguments std::pair<int, int>
     */
    IntegerPair getValues();

};

#endif // COMMANDLINE_H
