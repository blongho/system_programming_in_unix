#include "CommandLineHandler.h"

void CommandLineHandler::printError(const std::string prog, std::map<std::string, std::string> args)
{
    std::cerr << "Usage: " << prog << " ";
    for(const auto &key: args){
        std::cerr << key.first << " ";
    }
    std::cerr << std::endl;
    for(const auto &msg: args){
        std::cerr << msg.first << "  " << msg.second << '\n';
    }
    std::cerr << std::endl;
}

CommandLineHandler::CommandLineHandler(const int& args, char *argv[])
{
    process(args, argv);
}


void CommandLineHandler::process(const int& args, char *argv[])
{
    int decimals{};
    int iterations{};
    if(args > 1 && args <= 3)
    {
        std::string entry1{argv[1]};
        std::string entry2 = argv[2]== nullptr? "":argv[2];
        try
        {
            decimals = std::stoi(entry1);
            iterations = !entry2.empty()? std::stoi(entry2): 0;
            valuePair = std::make_pair(decimals, iterations);
            goodParameters = true;

        }
        catch(std::exception &e)
        {
            std::string prog = argv[0];
            prog = prog.substr(prog.rfind('/')+ 1 );
            std::cerr << "Bad entry: " << e.what() << std::endl;
            errorMap["arg1"] = "Number of decimal places(int)";
            errorMap["arg2"] = "Number of iterations(int)";
            printError(prog, errorMap);
        }
    }
    else
    {
        std::string prog = argv[0];
        prog = prog.substr(prog.rfind('/') + 1);
        errorMap["arg1"] = "Number of decimal places(int)";
        errorMap["arg2"] = "Number of iterations(int)";
        printError(prog, errorMap);
    }
}

IntegerPair CommandLineHandler::getValues()
{
    return valuePair;
}

bool CommandLineHandler::isGoodParameters()
{
    return goodParameters;
}
