#include <iostream>
#include <poll.h>
#include <cstdlib>
#include <cerrno>
#include <stropts.h>
#include <unistd.h>
#include <memory>
#include <vector>
#include <set>
#include<iomanip>
#include "PiCalculator.h"
#include "eCalculator.h"
#include "CommandLineHandler.h"

using std::vector;
using std::cout;
using std::endl;
typedef std::multiset<char> IntegerSet;

#define DEFAULT_NUMBERS 50000
#define PI_FILE "pi.dat" // file to hold the pi values
#define E_FILE "e.dat" // file to hold the e values
#define NUM_OF_FDS 2
#define BUFFSIZE  1024
#define TAB '\t'

int createFile(const char *path);
void informUser(string calc, int n, IntegerSet result);

int main(int args, char **argv)
{
    auto command = CommandLineHandler(args, argv);
    IntegerPair values{};
    int decimals{}, // number of decimal places for the calculation
    iterations{}; // number of times calculations will occur

    if(command.isGoodParameters()){
        values= command.getValues();
        decimals = values.first;
        iterations = (values.second == 0? DEFAULT_NUMBERS: values.second);
        cout << "Decimals " << decimals << " Number of iterations "
             << iterations << endl;

        int piFile = createFile(PI_FILE);
        int eFile = createFile(E_FILE);
        int fds[NUM_OF_FDS] = {piFile, eFile};


        if(pipe(fds) == -1){
            cerr << "Failed to create the pipe" << endl;
            return 1;
        }

        int i;
        int numnow{};
        char buf[BUFFSIZE];
        int bytesread{};
        struct pollfd *pfd;
        for (i=0; i< NUM_OF_FDS; i++){             /* initialize the polling structure */
            if (fds[i] >= 0)
                numnow++;
        }
        if ((pfd = (struct pollfd*)calloc(NUM_OF_FDS, sizeof(struct pollfd))) == nullptr){
            cerr << "Error creating the poll" << endl;
        }

        for (i = 0; i < NUM_OF_FDS; i++) {
            (pfd + i)->fd = *(fds + i);
            (pfd + i)->events = POLLRDNORM;
        }


        bool calcPi = true;
        pid_t pid = 0;
        int numready{};
        Calculator *piCalc = nullptr;
        Calculator *eCalc = nullptr;

        // Creating two childs.
        for (size_t i = 0; i < 2; i++)
        {
            pid = fork();

            if (pid == 0){
                break;
            }

            // after first fork, it will set the boolean to false.
            calcPi = false;
        }

        if (pid == -1)
        {
            perror("Failed the forking.");
            return 1;
        }

        // The child processes
        else if (pid == 0)
        {
            //cout << "Incide a child process with cpid " << getpid();
            // Doing the piCalc if calcPi is true. It's true in the first fork.
            if (calcPi)
            {
                // cout << " ==>doing pi stuff " << endl;
                for(int i = 0; i < iterations; i++){
                    piCalc = new PiCalculator(decimals, piFile);
                    piCalc->doCalc();
                    delete piCalc;
                }
            }
            // Doing the eCalc
            else
            {
                //cout << " ==>doing e stuff" << endl;
                for(int i = 0; i < iterations; i++){
                    eCalc = new eCalculator(decimals, eFile);
                    eCalc->doCalc();
                    delete eCalc;
                }

            }
        }
        // The parent process
        else
        {
            cout << "Inside parent " << endl;
            IntegerSet readData{};
            int readCounts{};
            while (numnow > 0) {        /* Continue monitoring until descriptors done */
                numready = poll(pfd, NUM_OF_FDS, -1);
                if ((numready == -1) && (errno == EINTR))
                    continue;                /* poll interrupted by a signal, try again */
                else if (numready == -1)            /* real poll error, can't continue */
                    break;
                for (i = 0; i < NUM_OF_FDS && numready > 0; i++)  {
                    if ((pfd + i)->revents) {
                        //cout << "Inside revents" << endl;
                        if ((pfd + i)->revents & (POLLRDNORM | POLLIN) ) {
                            bytesread = r_read(fds[i], buf, BUFFSIZE);
                            numready--;
                            if (bytesread > 0){
                                readCounts++; // count the number of reads
                                cout << "Bytes read " << bytesread << endl;
                                // convert char to string
                                string characters = std::to_string(bytesread);
                                // save chars into multiset
                                for(auto c: characters){
                                    readData.insert(c);
                                }
                                if(readCounts > 999 && (readCounts % 1000 == 0)){
                                    /**
                                     * TODO how to differentiate if it is pi or e
                                     */
                                    informUser("piCalculator", readCounts, readData);
                                }
                            }
                            else
                                bytesread = -1;                             /* end of file */
                        } else if ((pfd + i)->revents & (POLLERR | POLLHUP))
                            bytesread = -1;
                        else                    /* descriptor not involved in this round */
                            bytesread = 0;
                        if (bytesread == -1) {      /* error occurred, remove descriptor */
                            r_close(fds[i]);
                            (pfd + i)->fd = -1;
                            numnow--;
                        }
                    }
                }
            }
            for (i = 0; i < NUM_OF_FDS; i++){
                r_close(fds[i]);
                free(pfd);
            }
        }
    }
    return 0;
}

int createFile(const char *path){
    return open(path, O_RDWR|O_CREAT|O_APPEND, S_IRWXU);
}


void informUser(string calc, int n, IntegerSet result)
{
    vector<size_t> counts;
    counts.push_back(result.count(0));
    counts.push_back(result.count(1));
    counts.push_back(result.count(2));
    counts.push_back(result.count(3));
    counts.push_back(result.count(4));
    counts.push_back(result.count(5));
    counts.push_back(result.count(6));
    counts.push_back(result.count(7));
    counts.push_back(result.count(8));
    counts.push_back(result.count(9));

    size_t size = result.size();

    cout << endl;
    cout << calc << " n = " << n << endl;
    for(int i = 0; i <=9; i++){
        cout << i << TAB;
    }
    cout << endl;
    for(const auto &val: counts){
        cout << val << TAB;
    }
    cout << endl;

    for(const auto &val: counts){
        cout << std::setprecision(2) << std::fixed
             << static_cast<double>(val*100)/size << "%" << TAB;
    }
    cout << endl;
}

