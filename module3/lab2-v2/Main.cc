#include <poll.h>
#include <cstdlib>
#include <cerrno>
#include <stropts.h>
#include <unistd.h>
#include <memory>
#include "PiCalculator.h"
#include "eCalculator.h"
#include "CommandLineHandler.h"
#include "CalculatorPrinter.h"

using std::cout;
using std::endl;
using std::cerr;


int main(int args, char *argv[]) {
    // get the number of numbers to calculate places from arg, if not given set default
    CommandLineHandler commands = CommandLineHandler(args, argv);

    if(commands.isGoodParameters()){ //We have the values to run the program
        const unsigned DEFAULT_NUMBERS =  50000;
        size_t calculations{};
        IntegerPair intPair = commands.getValues();
        calculations = intPair.first == 0? DEFAULT_NUMBERS: intPair.first;

        cout << "Running " << argv[0] << " with " << calculations
             << " calculations" << endl;

        // the size of the buffer
        const int BUFSIZE = 4096;

        // buffer
        char buffer[BUFSIZE];

        // poll struct
        struct pollfd *pollfd;

        // create pipes
        int fd_piCalc[2];
        int fd_eCalc[2];
        if (pipe(fd_piCalc) == -1 || pipe(fd_eCalc) == -1) {
            cerr <<"failed to create pipe" << endl;
            return 1;
        }

        // create fd array
        int *fds[2];
        fds[0] = fd_piCalc;
        fds[1] = fd_eCalc;

        // If this variable is zero after a fork, the process is a child
        pid_t childPid;

        // first fork
        if ((childPid  = fork()) == -1) {
            cerr <<"Failed to create a process" << endl;
            return 1;
        }

        /* first child code */
        if (childPid == 0) {
            // create piCalculator
            PiCalculator(calculations, fd_piCalc[1]).doCalc();
            return 0;
        }

        // second fork, only done from parent (childPid > 0)
        if (childPid > 0) {
            if ((childPid = fork()) == -1) {
                cerr << "Failed to create a process!\n" << endl;
                return 1;
            }

            /* second child code */
            if (childPid == 0) {
                // create eCalculator
                eCalculator(calculations, fd_eCalc[1]).doCalc();
                return 0;
            }
        }

        /* parent code */
        if (childPid > 0) {
            const char* eFilename = "e.dat";
            const char* piFilename = "pi.dat";

            // open files to write to (pi.dat, e.dat)
            int piFile, eFile;

            mode_t fdmode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
            int createMode = O_WRONLY | O_CREAT | O_APPEND;

            if ((piFile = r_open3(piFilename, createMode, fdmode)) == -1) {
                cerr << "Failed to open a file" << endl;
                return 1;
            }
            if ((eFile = r_open3(eFilename, createMode, fdmode)) == -1) {
                cerr <<"Failed to open a file!" << endl;
                return 1;
            }

            // number of active pipes
            size_t numPipes = 2;

            // initialize poll struct
            if ((pollfd = (struct pollfd *) calloc(numPipes, sizeof(struct pollfd))) == nullptr) {
                cerr << "Error initializing poll struct" << endl;
                return 1;
            }

            for (size_t i = 0; i < numPipes; i++) {
                (pollfd + i)->fd = **(fds + i);
                (pollfd + i)->events = POLLRDNORM;
            }

            // strings that will contain pi and e when the calculations are done
            std::string pi, e;

            // Containers to hold the decimals as they are obtained.
            IntegerSet piValues; // std::set<int>
            IntegerSet eValues;

            // read from the pipes
            while (numPipes > 0) {
                // check how many pipes are ready
                int numReady = poll(pollfd, 2, -1);

                // if poll returns -1 and errno != EINTR an error occurred that wasn't a signal interruption
                if (numReady == -1 && errno != EINTR) {
                    cerr << "Polling failed" << endl;
                    return 1;
                }

                // if any pollfd structs are ready, check each one to see if return event is set
                for (int i = 0; i < 2 && numReady > 0; i++) {
                    ssize_t bytesRead;
                    if ((pollfd + i)->revents & (POLLRDNORM | POLLIN)) {
                        bytesRead = r_read(fds[i][0], buffer, BUFSIZE);
                        numReady--;
                        // if bytes were succesfully read
                        if (bytesRead > 0) {

                            char charsRead[BUFSIZE];
                            sprintf(charsRead, "%.*s", (int)bytesRead, buffer);

                            // write to file
                            r_write(i == 0 ? piFile : eFile, buffer, bytesRead);

                            // add read characters to correct string
                            for (int j = 0; j < bytesRead; j++) {
                                if (i == 0) { // this is comming from piPipe
                                    pi += charsRead[j];
                                    piValues.insert((charsRead[j] - 48));
                                    // for every 1000th character added to pi, print some stats
                                    if (pi.length() % 1000 == 0) {
                                        CalculatorPrinter p("PiCalculator", piValues, pi.length());
                                        p.informUser();
                                    }
                                    // if all characters are read
                                    if (pi.length() == calculations)
                                        numPipes--;
                                } else {
                                    e += charsRead[j];

                                    // store the int equivalent of the is value
                                    // into eValues set
                                    eValues.insert(charsRead[j]- 48);

                                    // for every 1000th character added to e, print some stats
                                    if (e.length() % 1000 == 0) {
                                        CalculatorPrinter p("eCalculator", eValues, e.length());
                                        p.informUser();
                                    }

                                    // if all characters are read
                                    if (e.length() == calculations)
                                        numPipes--;
                                }
                            }
                        } else {
                            // reading failed
                            bytesRead = -1;
                        }
                    } else if ((pollfd + i)->revents & (POLLERR | POLLHUP)) {
                        // reading failed
                        bytesRead = -1;
                    }

                    // if an error occurred
                    if (bytesRead == -1) {
                        perror("a read error occurred");
                    }
                } // end for loop
            } // end while(numPipes)
            free(pollfd);
            if (r_close(piFile) == -1 || r_close(eFile) == -1) {
                perror("Failed to close a file");
                return 1;
            }
            if (r_close(fd_piCalc[0]) == -1 || r_close(fd_eCalc[0]) == -1) {
                perror("Failed to close a pipe");
                return 1;
            }
            exit(EXIT_SUCCESS);
        } // end parent code

    }// END OF PROGRAM




    return 0;
}
