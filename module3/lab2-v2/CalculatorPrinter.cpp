#include "CalculatorPrinter.h"

CalculatorPrinter::CalculatorPrinter():
    calculation{}, result{} {}

CalculatorPrinter::CalculatorPrinter(const std::string &cal,
                                     const IntegerSet &intSet, int num)
    :calculation{cal}, result{intSet}, interval{num}{}


void CalculatorPrinter::informUser(std::ostream &os)
{
    const char TAB = '\t';
    const char LINE = '\n';
    vector<size_t> counts; // vector that hold the counts of 0, 1... 9

    for(int i = 0; i <= 9; i++){
		counts.push_back(result.count(i));
	}
 
    const size_t size = result.size();

    os << LINE << LINE;
    os << calculation << " n = " << interval << LINE;

	// print the header values 0, 1, 2, 3, ... 9
    for(int i = 0; i <=9; i++){
        os << i << TAB;
    }
    os << LINE;
	
	// print the counts of the values {num of 0s, num of 1s.. num of 9s)
    for(const auto &val: counts){
        os << val << TAB;
    }
   os << LINE;
	 
	// print the percentage of each value
    for(const auto &val: counts){
        os << std::setprecision(2) << std::fixed
             << static_cast<double>(val*100)/size << "%" << TAB;
    }
    os << std::endl;
}

