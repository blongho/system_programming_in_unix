/**
  * @file CalculatorPrinter.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @brief Prints a calculation values to screen
  *
  * */
#ifndef CALCULATORPRINTER_H
#define CALCULATORPRINTER_H

#include <set>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
using std::string;
using std::vector;
typedef  std::multiset<int> IntegerSet;

class CalculatorPrinter
{
private:
    string calculation; /*The calculator PiCalculator or eCalculator {string}*/
    IntegerSet result;  /*The values generated from the calculation in a set*/
    int interval;         /*The interval of print printe*/


public:
    CalculatorPrinter();
    CalculatorPrinter(const string &cal, const IntegerSet &intSet, int num);
    /**
     * @brief informUser Inform the user of what is happenning based on the three
     * parameters given above
     * @param os The output stream
     */
    void informUser(std::ostream &os = std::cout);
};

#endif // CALCULATORPRINTER_H
