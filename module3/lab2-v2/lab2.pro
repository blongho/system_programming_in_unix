TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    Calculator.cc \
    eCalculator.cc \
    MultiLengthInteger.cc \
    PiCalculator.cc \
    Restart.cc \
    CalculatorPrinter.cpp \
    CommandLineHandler.cpp \
    Main.cc

HEADERS += \
    Calculator.h \
    eCalculator.h \
    MultiLengthInteger.h \
    PiCalculator.h \
    Restart.h \
    CalculatorPrinter.h \
    CommandLineHandler.h
