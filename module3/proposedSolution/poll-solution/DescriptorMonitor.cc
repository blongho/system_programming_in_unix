/* File: DescriptorMonitor.cc
   Topic: Implementation of base class DescriptorMonitor
*/

#include <errno.h>
#include <sys/select.h>
#include <iostream>
#include "DescriptorMonitor.h" 
#include "Restart.h"


DescriptorMonitor::DescriptorMonitor(map<int,DescriptorEventHandler*> &aDescriptorMap, int aBufSize)
:iDescriptorMap(aDescriptorMap),iBufSize(aBufSize)
{ 

   iBuf = new char[aBufSize];
}

DescriptorMonitor::~DescriptorMonitor() {

   delete [] iBuf;
}






