/* File: PollMonitor.h
   Topic: File descriptor monitor using poll
*/


#ifndef POLLMONITOR_H
#define POLLMONITOR_H

#include <poll.h>
#include "DescriptorMonitor.h"


// Uses system call 'poll'

class PollMonitor : public DescriptorMonitor {
public:
   PollMonitor(map<int,DescriptorEventHandler*> &aDescriptorMap, int aBufSize);
   ~PollMonitor();
   
   void startMonitor();
private:
   struct pollfd *pollfdArr;  
   int nFD;  // original #fd i.e. the size of pollfdArr
};

#endif

