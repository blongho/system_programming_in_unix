/* File: ExecutionTimer.h 
   Topic: Definition of class ExecutionTimer which provides operations for
          execution timing.
*/

#ifndef EXECUTIONTIMER_H
#define EXECUTIONTIMER_H

#include <sys/time.h>

class ExecutionTimer {
public:
   ExecutionTimer() { };
   timeval start();  // Start timing
   timeval stop();	  // End timing
   void getElapsedTime(unsigned &aHour,
	               unsigned &aMin,
                       unsigned &aSec,
	               unsigned &aMs);
	
private:
   struct timeval iStartTime, iEndTime;
};

#endif 
