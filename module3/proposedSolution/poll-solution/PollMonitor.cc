/* File: PollMonitor.cc
   Topic: Implementation of class PollMonitor
*/

#include <errno.h>
#include <iostream>
#include "PollMonitor.h" 
#include "Restart.h"


PollMonitor::PollMonitor(map<int,DescriptorEventHandler*> &aDescriptorMap, int aBufSize)
:DescriptorMonitor(aDescriptorMap,aBufSize), nFD(aDescriptorMap.size())
{ 
   // Set up the range of descriptors to monitor 

   // it->first corresponds to the file descriptor
   // it->second corresponds to the DescriptorEventHandler  for the descriptor
      
   // Check descriptors and get the greatest fd
   Iterator start=iDescriptorMap.begin(), stop=iDescriptorMap.end(), it;
   
   for(it=start;it!=stop;++it) {
      if ((it->first  < 0) || (it->first  >= FD_SETSIZE)) {
         cerr << "Bad descriptor or too many descriptors.\n";
         return;
      }
   }   
   
 
   // Allocate the pollfd structs
   pollfdArr = new pollfd[nFD];
   
   // Initialize the pollfd structs
   start=iDescriptorMap.begin(), stop=iDescriptorMap.end();
   int i;
   for(i=0,it=start; it!=stop; ++i,++it) {
     pollfdArr[i].fd = it->first;
     pollfdArr[i].events = POLLRDNORM;
   }
}

PollMonitor::~PollMonitor() {
   // If there are open descriptors left, close them
   Iterator start=iDescriptorMap.begin(), stop=iDescriptorMap.end(), it;	 
   for (it = start; it!=stop; ++it) 
       r_close(it->first);
       
   delete [] pollfdArr;
}
   

void PollMonitor::startMonitor() {
      
   Iterator start, stop, it;
   int numNow = nFD;
   const int DATA_IS_READ = (POLLRDNORM | POLLIN);
   const int ERROR_OR_EOF = (POLLERR | POLLHUP);
   
   while (numNow > 0) {   // Continue monitoring until all are done 
      
      int numReady = poll(pollfdArr,nFD,-1);  // Which fd are ready? 
      if ((numReady == -1) && (errno == EINTR))  // Interrupted by signal, start over
         continue;   
      else if (numReady == -1)  {                 // Real poll error 
         perror("poll error, numReady -1");
         break; 
      }
      
      int bytesRead;
      for (int i=0; numReady>0 && i<nFD; ++i) { // Read and process
      
         int currFD = pollfdArr[i].fd;
         if(pollfdArr[i].revents) { // !=0 if events on this descriptor
 	    
	    if (pollfdArr[i].revents & DATA_IS_READ ) {

	        bytesRead = r_read(currFD,iBuf,iBufSize);

	        --numReady;
		if(bytesRead > 0) 
		   iDescriptorMap[currFD]->takeAction(iBuf,bytesRead);
		else
		   bytesRead = -1; // End of file
            } 
	    else if (pollfdArr[i].revents & ERROR_OR_EOF) {
	       r_close(currFD); // Error occurred or EOF
               pollfdArr[i].fd = -1; // poll will ignore this one next time
               numNow--;
	    }   
	 }  // if
      } // for
   }  // while
 }
     
      

