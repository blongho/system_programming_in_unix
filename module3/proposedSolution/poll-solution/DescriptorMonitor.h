/* File: DescriptorMonitor.h
   Topic: Base class for file descriptor monitors SelectMonitor and PollMonitor
*/

#ifndef DESCRIPTORMONITOR_H
#define DESCRIPTORMONITOR_H

#include <map>

using namespace std;

/* Abstract base for classes implementing the takeAction callback operation in the concrete monitors. 
   The takeAction member is called after each successful read.
*/
   
class DescriptorEventHandler {
public:
  DescriptorEventHandler() {};
  virtual ~DescriptorEventHandler() {} 
  virtual void takeAction(char aBuf[], int aCharRead)=0;
  virtual void show()=0;
  
};


/*  Abstract base for monitors using select or poll to monitor a set of file descriptors.
    First argument to constructor is a map with pairs where a descriptor is mapped to a DescriptorEventHandler 
    with its takeAction callback.
*/

class DescriptorMonitor {
public:
   DescriptorMonitor(map<int,DescriptorEventHandler*> &aDescriptorMap, int aBufSize);
   virtual ~DescriptorMonitor();
   
   virtual void startMonitor()=0;
   
protected:
   typedef map<int,DescriptorEventHandler*> FDmapType;
   typedef FDmapType::iterator Iterator;

   map<int,DescriptorEventHandler*> &iDescriptorMap; // fileDescriptor -> eventHandler
   char *iBuf;	// Read buffer
   int iBufSize;
};


#endif




