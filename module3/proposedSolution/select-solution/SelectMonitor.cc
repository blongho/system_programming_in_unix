/* File: SelectMonitor.cc
   Topic: Implementation of class SelectMonitor
*/

#include <errno.h>
#include <sys/select.h>
#include <stdio.h>
#include <iostream>
#include "SelectMonitor.h" 
#include "Restart.h"



SelectMonitor::SelectMonitor(map<int,DescriptorEventHandler*> &aDescriptorMap, int aBufSize)
:DescriptorMonitor(aDescriptorMap,aBufSize),maxFD(0)
{ 
   // Set up the range of descriptors to monitor 
   
   
   // Get the greatest fd
   Iterator start=iDescriptorMap.begin(), stop=iDescriptorMap.end(), it;
   for(it=start;it!=stop;++it) {
      if ((it->first  < 0) || (it->first  >= FD_SETSIZE)) {
         cerr << "Bad descriptor or too many descriptors.\n";
         return;
      }
   
     if( it->first > maxFD)
       maxFD = it->first;
   }   
}

SelectMonitor::~SelectMonitor() {
   // If there are open descriptors left, close them
   Iterator start=iDescriptorMap.begin(), stop=iDescriptorMap.end(), it;	 
   for (it = start; it!=stop; ++it) 
       r_close(it->first);
}
   

void SelectMonitor::startMonitor() {
   
   fd_set readSet;
   Iterator start, stop, it;
 
   while (iDescriptorMap.size() > 0) {   // Continue monitoring until all are done 
      FD_ZERO(&readSet);         // Set up the file descriptor mask 
      start=iDescriptorMap.begin(),stop=iDescriptorMap.end();
      for(it=start;it!=stop;++it)
          if(it->first >= 0) 
	    FD_SET(it->first, &readSet);

      int numReady = select(maxFD+1, &readSet, NULL, NULL, NULL);  // Which fd are ready? 
      if ((numReady == -1) && (errno == EINTR))  // Interrupted by signal, start over
         continue;   
      else if (numReady == -1)  {                 // Real select error 
         perror("select error, numReady -1");
         break; 
      }
      for (it = start; (it!=stop) && (numReady>0); ++it) { // Read and process
      
         if (it->second == NULL)                    // This descriptor is done,
            continue;                               // ...get next
         if (FD_ISSET(it->first, &readSet)) {        // This descriptor is ready 
            int bytesRead = r_read(it->first, iBuf, iBufSize);
            numReady--;
            if (bytesRead > 0)
               it->second->takeAction(iBuf, bytesRead);
            else  {           // Error occurred on this descriptor, close it 
	cout << "bytesRead: " << bytesRead << endl;
               r_close(it->first);
	       iDescriptorMap.erase(it);
	       break;  // iDescriptorMap changed, start over
            } // else
         }  // if
      }  // for
   }  // while     
}

