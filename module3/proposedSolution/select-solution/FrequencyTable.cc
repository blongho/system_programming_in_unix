#include <iomanip>
#include "FrequencyTable.h" 


FrequencyTable::FrequencyTable(int aCols, int aStartCol, string aLabel)
:iCols(aCols), iStartCol(aStartCol), iLabel(aLabel), iCount(0)
{
  iTable = new int[iCols];
  for(int i=0;i<iCols;++i)
    iTable[i] = 0;
}
  
FrequencyTable::~FrequencyTable() {
  delete [] iTable;
}

void FrequencyTable::addOne(int col) {
  ++iTable[col];
  ++iCount;
}

void FrequencyTable::show(ostream &os) {
  int sum=0;
  os << '\n' << iLabel << " n = " << iCount << ":\n";
  for(int i=0; i<iCols;++i)
     os << setw(8) << i+iStartCol;
  os << '\n';
  for(int i=0; i<iCols;++i) {
     os << setw(8) << iTable[i];
     sum+=iTable[i];
  }   
  os << "\n ";
  for(int i=0; i<iCols;++i) {
     os << setw(7) << fixed << setprecision(2)<< iTable[i]/(float)sum*100.0;
     os << '%';
  }
  os << endl;    
  
}   

void FrequencyTable::reset(){
  for(int i=0;i<iCols;++i)
    iTable[i] = 0;
}
