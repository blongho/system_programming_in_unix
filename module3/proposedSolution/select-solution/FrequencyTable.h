#ifndef FREQUENCYTABLE_H
#define FREQUENCYTABLE_H

#include<iostream>

using namespace std;

class FrequencyTable {

public:
   FrequencyTable(int aCols, int aStartCol, string aLabel="");
   ~FrequencyTable();
   void addOne(int col);
   void show(ostream &os=cout);
   void reset();
private: 
   int *iTable;
   int iCols;
   int iStartCol;
   int iCount;
   string iLabel;
};

#endif




