/* File: SelectMonitor.h
   Topic: File descriptor monitor using select
*/


#ifndef SELECTMONITOR_H
#define SELECTMONITOR_H

#include "DescriptorMonitor.h"


// Uses system call 'select'

class SelectMonitor : public DescriptorMonitor {
public:
   SelectMonitor(map<int,DescriptorEventHandler*> &aDescriptorMap, int aBufSize);
   ~SelectMonitor();
   
   void startMonitor();
private:
   int maxFD; 
};

#endif

