/* File: ExecutionTimer.cc
   Topic: Implementation of class ExecutionTimer.
*/
#include <cstdio>
#include "ExecutionTimer.h"

timeval ExecutionTimer::start() {
   gettimeofday(&iStartTime,NULL);
   return iStartTime;
}


timeval ExecutionTimer::stop() {
   gettimeofday(&iEndTime,NULL);
   return iEndTime;
}


void ExecutionTimer::getElapsedTime(unsigned &aHour,
	                            unsigned &aMin,
                                    unsigned &aSec,
	                            unsigned &aMs) {				   

   long timeDiffInMS = 
   1000*(iEndTime.tv_sec - iStartTime.tv_sec)+
                               (iEndTime.tv_usec-iStartTime.tv_usec)/1000;
			       
   aHour = timeDiffInMS/3600000;
   unsigned msLeft = timeDiffInMS - (aHour*3600000);
   aMin = msLeft / 60000 ;
   msLeft = msLeft % 60000;
   aSec = msLeft/1000;
   aMs = msLeft % 1000;			       

}



