#include <unistd.h>
#include <map>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <sys/wait.h>
#include "eCalculator.h"
#include "PiCalculator.h"
#include "FrequencyTable.h"
#include "SelectMonitor.h"
#include "Restart.h"
#include "ExecutionTimer.h"

using namespace std;

const int READ=0, WRITE=1;


template<typename T> 
T fromString(const string str) throw (invalid_argument) {
   stringstream ss;
   T value;
   ss << str;
   ss >> value;
   if(!ss) throw invalid_argument("Exception in fromStr: conversion failure.");
   return value;
}


// Will receive and handle the digits sent from a pipe
class DigitEventAction : public DescriptorEventHandler {
public:
  DigitEventAction(string aLabel,int aDigits);
  virtual ~DigitEventAction();
  virtual void takeAction(char aBuf[], int aCharRead); 
  virtual void show();
  
private:
  int iCount;
  int iNdigits;
  string iLabel;
  FrequencyTable aFreqTbl;
  ofstream iFile;
};


DigitEventAction::DigitEventAction(string aLabel,int aNdigits)
:iCount(0),iNdigits(aNdigits),iLabel(aLabel),aFreqTbl(10,0,iLabel)
{
   stringstream ss;
   ss << iLabel << iNdigits << ".dat";
   string fName;
   ss >> fName;
   iFile.open(fName.c_str(),ios::out|ios::trunc);

}

DigitEventAction::~DigitEventAction() {
   iFile.close();
}

void DigitEventAction::takeAction(char aBuf[], int aCharRead) {
  for(int i=0; i<aCharRead; ++i) {
     aFreqTbl.addOne(aBuf[i]-'0');
     iFile << aBuf[i];
  }
  iCount+=aCharRead; 
  if((iCount%500)==0)
     aFreqTbl.show();		
}

void DigitEventAction::show() {
  aFreqTbl.show();
}

int main(int argc, char* argv[]) {

  if(argc!=2) {
    cout << "Usage: " << argv[0] << " #decimal places" << endl;
    exit(1);
  }
   
  int nDecimalPlaces;	
	  
  try {
    nDecimalPlaces = fromString<int>(argv[1]);
  }    
  catch(exception &eo) {
    cout << eo.what() << endl;
    cout << "Please check the argument." << endl;
    return 0;
  }

  pid_t childPid;
  int ePipeFd[2], piPipeFd[2];
  
  map<pid_t,string> pidMap;  // map pid -> process description
  map<int,DescriptorEventHandler*> digitEventHandlerMap;  // map read pipe -> handler for data
  DescriptorEventHandler *piDigitEventHandler, *eDigitEventHandler;  // Pipe readers
  
  
  if(pipe(ePipeFd)==-1) {
    perror("Failed to create eCalculator-pipe");
    return 1;
  }
  if(pipe(piPipeFd)==-1) {
    perror("Failed to create eCalculator-pipe");
    return 1;
  }
  

   // Setup eCalculator process  
  if((childPid=fork())==-1) {
    perror("Failed to fork eCalculator-process");
    return 1;
  }
  else if(childPid==0) {
  // eCalculator process, writes the digits to ePipeFd[WRITE]  
    close(ePipeFd[READ]);
    Calculator *eCalc = new eCalculator(nDecimalPlaces,ePipeFd[WRITE]);  
    eCalc->doCalc();
    close(ePipeFd[WRITE]);
    delete eCalc;
    exit(0);  
  }
  else {  // parent
    close(ePipeFd[WRITE]);
    pidMap[childPid]="e-calculator";
    eDigitEventHandler = new DigitEventAction("eCalculator",nDecimalPlaces); // Handler for digits from eCalculator
    digitEventHandlerMap[ePipeFd[READ]] = eDigitEventHandler;  
  }
 

 // Setup piCalculator process  
  if((childPid=fork())==-1) {
    perror("Failed to fork piCalculator-process");
    return 1;
  }
  else if(childPid==0) {
  // piCalculator process, writes the digits to piPipeFd[WRITE]  
    close(piPipeFd[READ]);
    Calculator *piCalc = new PiCalculator(nDecimalPlaces,piPipeFd[WRITE]);  
    piCalc->doCalc();
    close(piPipeFd[WRITE]);
    delete piCalc;
    exit(0);  
  }
  else {  // parent
    close(piPipeFd[WRITE]);
    pidMap[childPid]="pi-calculator";
    piDigitEventHandler = new DigitEventAction("piCalculator",nDecimalPlaces); // Handler for digits from piCalculator
    digitEventHandlerMap[piPipeFd[READ]] = piDigitEventHandler;  
  }

  ExecutionTimer eTimer;
  eTimer.start();  // Start timing
  
  DescriptorMonitor *selMon = new SelectMonitor(digitEventHandlerMap,10);
  selMon->startMonitor();
 
  eTimer.stop();
   
  for(int i=0; i<pidMap.size();++i) {
    int status;
    pid_t pid = r_wait(&status);
    if(WIFEXITED(status)) 
      cout << "PID " << pid << ' ' << pidMap[pid] << " finished normally with exit code " << WEXITSTATUS(status) << endl;
    else
      cout << "PID " << pid << ' ' << pidMap[pid] << " finished abnormally." << endl;
  } 
  
 
  
   piDigitEventHandler->show();
   eDigitEventHandler->show();
  delete selMon;
  delete piDigitEventHandler;
  delete eDigitEventHandler;
  
 
  close(ePipeFd[READ]);
  close(piPipeFd[READ]);

  unsigned h,m,s,ms;
  eTimer.getElapsedTime(h,m,s,ms);
  cout << "Execution time: "<<h<<"h "<<m<<"m "<<s<<"s "<<ms<<"ms"<<endl; 
  
  return 0;
}
 


